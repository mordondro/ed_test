@�  �S0�`    )   L`       ��
9A?
4`�K`        Df             �(  &�(�&�&�Y���&�(�&�&�Y���� 0Rd    
       �                      ����
5a`    ����Da    ��     �� 
10�a
       @   �a      @@�  �K`        Dx0             � �%� &��&�(� &�(�&�})&��/��/��	/�
	�/��/��/��/��/��	/��
/��/��/��/��/� �!/�"!�#/�$#�%/�&%Z����'%��   ��d)      0������     �a    @@�  �
 >K`        Dl0             �z  %&�&�&�]�1��&�&�]�1��&�&�]�1��'��� &�(�	&�Z���&�r�
%�-%�� (Rc           ¹                 Qa�    352     `    ����Da    !�     �� �b      � ��@       
a7a       �  $
1A&$
1�1$
5�$
5�	$
5�$
5$
5�$
5�$
5A$$
5�'$
5A+$
5�.$
5A2$
5�7$
9 $
9�$
9!$
9a&$
9�)$
95$
9�8$
9<$
=�$Sh�`    �   L`       �`        $Lg                                                               SH�`    Y   @L`       @Rc           �                 ��
 
Ab    �      R        I`    ����Da    Q�     ,� 

a<�a              
!!.Qb7��   c5bgx3h0lcjhqsm 
�A?CQaVh6V   getTokenC
!a3C
!�3C
!a4CS��`    �  �L`        
 !&
t�;Qa.S_�   hash    �Qc�#L   dropbox-csrf-token      Qc����   db-restore-doc-path     Qa��<�   db-cid  Qc
f�   access_token\=([^\&]+)  �Qd��	   state\=dropbox_auth_([^\&]+)    M
%�
|�
\�6$QgZ9�5   Cross site forgery detected on Dropbox authorization!   
%aIQbV��2   dropbox-token   �a              
%�G
!!.
t8
p!6
(
%A
!a4I�a              
%�G
%!1
%�1
%�1
A<K`        DAh            �  &�(�&�(�&�&�(�&�&�Y���&�&�(�&�&�Y���
&�&�(�&�&�Y���&�y &�(�&�Y���&�y	 &�(�&�Y���&�%��%��{(�
&�j��n%��j(�
&�j��]&�(�&�&�Y��� *�"h�$�%&�(�'&�&�Y���)�*�+--&�(�/&�&�(1&�}3)&�W��4(6�0(8&�%�h�:'�&�(�&�&�Y���;�&�-�=%�?&�(�u may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).