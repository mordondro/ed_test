
<p style="font-size: 2em;font-weight: bold;">
    the edddison solution - Introduction
</p>

## edddison solution

The edddison solution consists of multiple parts. 

The *minimum* you need for edddison to work are:

* the edddison software
* the edddison plugin 
* one edddison controller

### the edddison software

The edddison software is the core of the edddison solution and has to be installed on all systems, which are using edddison, *also if its only for the presentation of your project.*

You can download the edddison software from our website.
[Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)

![](img/intro/1000000000000344000001DCF2AC0A3D427DBEA1.jpg)

Each supported program needs its own edddison software installation.
For example:

**You are using edddison for Unity**
You will have to install:

* edddison *software* for *Unity*
* edddison *plugin* for *Unity*

**You are using edddison for Unreal AND for Unity**

* edddison *software* for *Unity*
* edddison *plugin* for *Unity*
* edddison *software* for *Unreal*
* edddison *plugin* for *Unreal*

### the edddison plugin

The *edddison plugin*, which handles the connection between your 3D Software (e.g. Unity, Unreal, ...) and the *edddison software*.

Depending on your 3D Program you can download it their 3D Extension Markets.

* Edddison unity plugin - [Asset Store](<https://assetstore.unity.com/packages/tools/input-management/walk-through-controller-73575>)
* edddison sketchup plugin - [Extension Warehouse](<https://extensions.sketchup.com/en/content/edddison>)
* edddison 3ds Max plugin - ??? 
* edddison PlantSim plugin - ??? 
* edddison unreal plugin - [UE4 Marketplace](<https://www.unrealengine.com/marketplace/en-US/slug/edddison-create-interactive-3d-presentations/reviews>)
* edddison Navisworks plugin - ??? 

### the edddison controllers

The present or for the users to interact with your project you need one or multiple edddison controllers. These are the available in different versions

#### Software controllers

You can interact with edddison by using one of the following software solutions:

* edddison controller for *Android*
* edddison controller for *iOS*
* edddison controller *in your browser* (touch device recommended)
* edddison *VR Controller*

![](img/intro/softwarecontroller.png)
*edddison controller app*

More Information can be found in the respective tutorial.

#### Hardware Controllers

In addition to the software controllers edddison supports hardware solutions to present your project.

*To control your 3D software with tangible objects, you can* use the **edddison projection table** , **edddison tabletop** or **edddison for HP Sprout** in combination with *tangible objects*.

![](img/hardware/10000000000002CD000001C2A2EF94EFC396DC40.jpg)
*edddison tabletop kit*

An *edddison marker* is a printed label (like a barcode) that is attached on a control object. The markers are read by a camera. Adding markers to any tangible object generates a control object that can be used in edddison. These real figures can be produced with a 3D printer, a laser cutter or by adding markers to existing figures (e.g. toys).

By moving these tangible objects on your floor plan, you can control objects within the 3D scene.

![](img/intro/1000000000000800000006008BF37431F96E90EF.jpg)
*edddison projection table*

You can *buy* hardware controllers like the *edddison tabletop kit* or you can *build* your own hardware controller.

Explanations on how to do that can be found in the respective Tutorials.

* edddison *tabletop*
* edddison *photobook* for tabletop
* edddison *projection table*
* build a projection table

## Getting Started

To ensure a smooth workflow while setting up your project we suggest you to follow the following steps:

1. Download and install the edddison software (See above)	
2. Follow the Tutorial *Plugin* for your 3D Software
3. Follow the Tutorial *editor*
4. Follow the Tutorial *controller*

### Supported 3D Software

edddison is available for the following programs:

* Unreal Engine
* Unity
* Autodesk Navisworks
* Trimble Sketchup
* Tecnomatix Plant Simulation
* 3ds Max Interactive

## edddison demos

##### End User presentational files

Want to test edddison without building your own project?
We prepared ready-to-use presentations for you to test edddison.

***Note**: This approach works for the demos of **unity, unreal and 3dsmax interactive**. For the other demos you will have to install the respective 3D Software and the plugin.

Follow this instructions:

1. Visit the Demo-Files-Section at https://www.edddison.com/downloads-demo-2/
2. Go to *End User presentational files for PC*
3. Download the demo file for the 3D Software you would like to test.
4. Unzip the demo files
5. Register at edddison.com
6. Go to *Account->Licenses and Downloads*
7. Download the right edddison software according to the demo file you have chosen.
8. Click on *Download License* to obtain a 14-Day Trial License for your product.
9. Execute the setup and follow the instructions.
10. Start the Demo files .exe

##### Developer project files
	
Use this files to follow the respective tutorials for the edddison plugins.

## edddison Video tutorials

Our Video can be found on [Youtube ](https://www.youtube.com/playlist?list=PLqCpnh2wkMWcsrkfiMl2npGcqyOTqgRzG)
We try to update and extend them continously.