## IMPORTANT NOTE

For edddison to run, you will always need this 4 parts:

1. The 3D software you are using (e.g. Unity, 3DSMax, Sketchup, Unreal, etc.)
2. The *edddison plugin* corresponding to your 3D Software
3. The *edddison editor* Software corresponding to your 3D Software
4. The *edddison license* corresponding to your 3D Software

If you use edddison with more than one 3D Software you will also need 
* two edddison plugins
* two eddison editor installations
* two edddison editor license files

For more information on this see the Intro (LINK)

## Download and install the "edddison editor" software

1. Login or Create an account at  [edddison.com](<https://edddison.com/index.php/customer/account/create/>) 
2. Search the right software version, depending on the plugin you want to use. 
3. Download Software
4. Execute Setup and follow the instructions

***Note:** the edddison editor is not being started directly by clicking on the .exe file, therefore you will not find any desktop icon or similiar.


## Start edddison editor

### Start edddison from your 3D application

***Note:** the edddison editor is not being started directly by clicking on the .exe file, therefore you will not find any desktop icon or similiar.

**If you arrived here by following one of the edddison plugin tutorials please skip to the next step

For instructions on how to get edddison started please follow the corresponding instructions and then come back here:

Unity3D: see **here**

Unreal Engine: see **here**

Autodesk Navisworks: see **here**

Autodesk 3ds Max Interactive: see **here**

Sketchup: see **here**

Tecnomatix Plant Simulation: see **here**


### Add a license

After you've started edddison editor for the first time, you will need to add a license file.

Each 3D Program needs its own *edddison editor software*-license, same as its own *edddison editor software*-product (described above).

***Note**: If you didnt buy a edddison software license, there are separate trial licenses and licenses for students available at our [homepage](www.edddison.com)

***Note:** You need at least one license to make edddison work.

![](img/editor/10000000000004EF000002DAAD0A8588763CD109.jpg)

#### Download license

After you log in at [edddison.com](www.edddison.com) you can find all available setup and license files. 
To make edddison work, you have to download a setup plus the corresponding license file. (for more info see above)

***NOTE**: At first only the trial versions will be listed there. 
As soon as you buy new software, you will find the software plus the license in the list as well.**

![](img/editor/1000000000000510000002E39712F0699B9CBB9A.jpg)

#### Load license

When starting edddison for the first time, the program will ask you for your license file. 
Simply click on the *Load license file* button, search for your download directory and select the right *license.dat* file.


#### Add new license

If you want to add another license or exchange the trial version for a purchased license, select *Help \> Add license*. There you will find the same dialogue as the one when adding your first license.

![](img/editor/1000000000000502000002DCB5010C77D8FD341E.jpg)

## Open project

You can start the edddison editor by starting a new project from scratch (following the tutorial),
or you can use one of our demo projects.

To follow the tutorial jump to the **Step 3.2**

### Using the Demo Files

#### Downloading Demo Files

 ***Note:** If you already downloaded a demo project from our website while following our tutorials, jump the this step.*

 ![](img/editor/1000000000000697000003B21B54706B83C5BAE8.jpg)

Use one of the demo projects found on our homepage to start edddison. [*edddison.com/downloads-demo-2/*](https://www.edddison.com/downloads-demo-2/)

1. Download 
2. Unzip the file, open it in Unity, make a build and run it.
3. edddison will start automatically.


####  Import edddison configuration

Use the **Import Project** button to import the edddison
configuration for the downloaded demo project into edddison. Then select
it and click on the Open button.

![](img/editor/1000000000000502000002DCCA337950A9C1811C.jpg)


### Create new edddison configuration

Select **Create** in the edddison configuration dialog to create a new
configuration. You can also import or export existing configurations.

***Note:** edddison saves your configuration automatically.*

If you want to edit the name or copy an existing configuration, click
the corresponding buttons.

## The Editor

The Editor consists of four different areas.

* Object-Area: Create new edddison objects
* The Floor Plan: The interactive emulation area for your final controller view. 
* Created-Objects: Activate/Deactivate and delete created edddison objects
* Properties: The properties of the selected objects

![](img/editor/bild1.jpg)

When you start a new project, you will see a black Floorplan area and an empty "Created Objects"-area.

![](img/editor/emptyproject.jpg)


Building your edddison project consists of the following parts:

* Link the floor plans (bounding boxes) you created in your 3D-Software
* Link the Cameras you created in your 3D Software to create your POVs
* Link the movable and stationary 3D objects you created in your 3D Software
* Add external media to make your presentation more interactive
* Test your edddison project in real-time with the edddison editor and your 3D-Model
* Use edddison with your controller devices (Internet Browser, Smartphone, Tablet, MRI)

To get started lets first add our floor plans.

### Floor plan / bounding box

#### Add floor plan

Use the **create floor plan** button to add a floor plan to your project.

Select a bounding box, the corresponding floor plan image and a unique name.

![](img/editor/selectfloorplan.jpg)

***Note:** If you dont see any objects in the dropdown-list, either try to close the application and run it again or make sure that you added your objects as "edddison objects" in your 3D-Model, so the editor knows which objects you want to be interactive.

#### Add a floor plan image

Now we need to import import the 2D Floor plan, that we created from our 3D-Model before.

* Click **Import Image** and select your 2D floor plan image. (Supports .jpg and .png)

You can always change the image of the floor plan if you wish to do so by importing a new image.

The floorplan area should now show the imported image.

![](img/editor/selectfloorplan2.jpg)


**Repeat this two steps (add floor plan / add image) for all bounding boxes in your 3D-Model.

***Note Tecnomatix Plant Simulation:** When you are using Tecnomatix Plant Simulation you can automatically generate a floor plan image by clicking **Generate Image**


#### Set the camera height

***Note**: To see any changes you  need to have created a POV-Camera first. Please come back to this step after you created a POV.

You can set the *camera height*-setting by setting the *camera height* individually for each bounding box. Thus, you set the height once for the rooms and all cameras apply to it automatically.


![](img/editor/cameraheight.jpg)


### Point of View - POV (Cameras)

In order to interact with your scene, you need to create a **Point of View (POV)**.

There are two different POVs in edddison:

* **POV (First Person):** This basically acts as the spectators eyes and allows him to walk through the model in a First-Person-View. It has a given height (eye height).

* **Target Point of View (Look-at-Camera):** This camera acts as an 360°-overview camera with a fixed target. Typically you would use this camera to look at a defined target like a 3D Object and pivot around it. The Yaw will be set automatically.

***Important Note:** You always have to add a floor plan before you can add a point of view.*

#### POV Camera (First Person)

In order to walk around your scene as a person, you need to create a *POV-Camera*.

* Click **Create point of view** from the Objects-Area
* In the second screen choose a camera from your 3D-Model. Typically there is only one camera.
* Choose **Point of View**
* Name your Camera

![](img/editor/selectpov.jpg)

![](img/editor/selectpov2.jpg)

You can test your *POV-cam* by:
* Clicking and dragging your left mouse-button on top of red marker in the floor plan area
* While hovering over the red marker turn your mouse-wheel to change view-direction

You will see the changes in real-time in your 3D-Model


![](img/editor/testpov1.jpg)

To tweak the settings of the camera please refer to the chapter **Camera Settings**.

***Note:** If you dont see any objects in the dropdown-list, either try to close the application and run it again or make sure that you added your objects as "edddison objects" in your 3D-Model, so the editor knows which objects you want to be interactive.

#### Target Point of View (Look-at-Camera)

The **Look-at-Camera** by default looks at the midpoint active floor plan(bounding box), giving you the possibility to pivot around your bounding box in a 360°-View.
This is ideal if you want to walk around a house, for example.

When you have added 3D-Objects to your scene, you will also be able to set the Cameras Target to a specific object, pivoting it instead. Then the camera will always focus on this object and you can easily walk around it.

* Click **Create point of view** from the Objects-Area
* In the second screen choose a camera from your 3D-Model. Typically there is only one camera.
* Choose **Target point of View**
* Name your Camera

![](img/editor/selectpov.jpg)

![](img/editor/selectpov3.jpg)

You can test your *Look-at-Camera* by:
* Clicking and dragging your left mouse-button on top of red marker in the floor plan area

You will see the changes in real-time in your 3D-Model

![](img/editor/testpov2.jpg)

To tweak the settings of the camera please refer to the chapter **Camera Settings**.

#### POV-Settings

* Click on the camera in the **Created Objects**-area to open the properties.

**Tilt**: Turn the camera so you look up or down.

**Field of view**: Change how big your field of view should be -- the wider your field of view is, the farther away you seem to be.

**Roll**: Rolls your view sideways

 **Damping:** Here you can set the smoothness of the camera rotation and translation, when the user is moving them.

***Height:** The height of the camera depends on the bounding box; therefore you can set it in the properties of each bounding box individually. (See Chapter "Floor Plans - Height setting")

 ![](img/editor/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)
 
### Viewpoints

**Viewpoints** are edddison objects, that save the current state of your camera its own object, so you can easily get back into the same position.

This can be used to give the user easy access to focus on specific area of the 3D Model.

The Viewpoint will save:
* Cameras position
* Tilt & Roll Settings

To **create a Viewpoint** you have to:

1. Set the camera to the desired position and adjust Tilt & Roll
2. Click **Create Viewpoint** from the Objects-Area
3. Give the Viewpoint a Name

 ![](img/editor/viewpoint.jpg)

After you created the viewpoint it will appear in your *Created-Objects*-area.

In the Viewpoint-properties you can:

* Add a thumbnail to be shown in the controller apps. (for example an image of the cameras view)
* Update the Viewport (Move the camera to a new position and clicking "update viewport")

To test your viewport move the POV-Marker in the floorplan area and then click two times on the **eye-symbol** of the viewpoint-object in the *Created-Objects*-area. (e.g. deactivating and activating the object).
This will set your view in the 3D-Model to the viewpoint.

***Note:** You can create as many viewpoints as you desire.

***Note Tecnomatix Plant Simulation:** When you are using Tecnomatix Plant Simulation you can automatically generate a floor plan image by clicking **Generate Image**


### 3D Objects

You can add **3D objects** the same way as *point of views* and *floor plans*.

To link a 3D Object, that you *marked as edddison object* in your 3D Software, choose first which kind of edddison object you want it to be assigned to.

Objects in edddison are eiter *stationary* or *movable*.

* **Stationary Object:** User can activate/deactivate the object on the floorplan. They will not loose their position.
* **Movable Object:** User can activate/deactivate the object move it freely on the floorplan.

***Note:** edddison objects are not respective to floorplans. You can use each object on all created floor plans.
***Note:** All 3D objects can also be used as a target point of view for your Look-at-Camera

### Stationary 3D objects

#### Create Stationary 3D objects

1. Click **Create stationary 3D Object** from the Objects-Area
2. Choose the 3D Object you want to add as stationary.
3. Choose a Name

![](img/editor/create3dobject1.jpg)

![](img/editor/create3dobject3.jpg)

***Note :** The object will not appear in the floor plan area, since its stationary and does not have any interactive moving property. You can activate and deactivate the object via the **Created-Objects**-area (eye-symbol)

#### Stationary 3D Object settings

In the 3D Object-properties you can:

**Thumbnail:** Add a thumbnail to be shown in the controller apps. (for example an image of the object)
**Point of view target:** Make the object the target of the *Look-at-Camera*. (see chapter *Point of View*)

![](img/editor/objectprop.jpg)

***Note Tecnomatix Plant Simulation:** When you are using Tecnomatix Plant Simulation you can automatically generate a floor plan image by clicking **Generate Image**

### Movable 3D objects

#### Create movable 3D objects

1. Click **Create movable 3D Object** from the Objects-Area
2. Choose the 3D Object you want to add as movable.
3. Choose a Name

![](img/editor/create3dobject2.jpg)

![](img/editor/create3dobject4.jpg)

Your new object will appear in the *Created Objects*-area and also as a interactive object in the floor plan view.

![](img/editor/movableobject1.jpg)

You can test your 3D Object by:
* Clicking and dragging your left mouse-button on top of the green marker in the floor plan area
* While hovering over the green marker turn your mouse-wheel to change view-direction


#### Movable 3D Object settings

* **Thumbnail:** Add a thumbnail to be shown in the controller apps. (for example an image of the object)

* **Point of view target:** Make the object the target of the *Look-at-Camera*. (see chapter *Point of View*)

* **Move horizontally/vertically:**: Restricts the object to be moved only along one axis on the floorplan.

* **Control height:**: Adjust the height distance of the object from the floorplan.

* **Control yaw:**
  
 * **Control pitch/roll:** Adjust the pitch and roll object.
 
  * **Freeze visibility:** Object can be moved but not turned on/off

  * **Control damping:** 
  
  
 ![](img/editor/1000000000000170000002981BF15A65BA5ABDBC.jpg)

 ***Note for Tecnomatix Plant Simulation:** When you are using Tecnomatix Plant Simulation you can automatically generate a floor plan image by clicking **Generate Image***
 
## External Media

edddison offers you the possibility to add external media to your interactive presentation:

**Supported Media:**

* **Images**
* **Videos**
* **Slideshows**

This media can be accessed easily and interactively via the controller apps and will be shown as an overlay of the 3D Model.

### Images

Supported file types are:  .jpg, .jpeg, .png or .gif

To add images to your presentation:

1. Click **Add Image** from the Objects-Area
2. Choose the Image via the **Load File**-Button
3. Choose a *heading text* for your image to be shown (optional)

 ![](img/editor/1000000000000502000002DC20A3E93C6BF9D9B1.jpg)

To test your image click on the **eye-symbol** of the image-object in the *Created-Objects*-area.
To deactivate the image you can use the **ESC-Button** on your keyboard.

Repeat this steps with as many images as you like.

***Note:** To change the Behaviour of the media viewer (e.g. the target monitor, scaling, background, etc. ) go to the media settings in the main menu (see more in the chapter media settings)

***Note:** If you want to show a lot of images at once, have a look at the **Slideshow**-option instead of adding single images.

### Videos

To add videos to your presentation:

1. Click **Add Video** from the Objects-Area
2. Choose the Image via de **Load File**-Button
3. Choose a *heading text* for your image to be shown (optional)

![](img/editor/1000000000000502000002DCF15727C1DDF075D8.jpg)
![](img/editor/addvideo.jpg)

***Note:** Videos are only possible in .mp4 and .flv format. For troubleshooting please refer to the tips in the wizard.

To test your video click on the **eye-symbol** of the image-object in the *Created-Objects*-area.
To deactivate the image you can use the **ESC-Button** on your keyboard.

Repeat this steps with as many videos as you like.

***Note:** To change the Behaviour of the media viewer (e.g. the target monitor, scaling, background, etc. ) go to the media settings in the main menu (see more in the chapter media settings)


### Slideshows

Slideshows consist of single pictures (.jpg, .jpeg, .png or .gif) that can be arranged in edddison. You can give each picture a different heading text. It will be shown above the picture during the presentation.

To add a slideshow of multiple images to your presentation:

1. Click **Add Slideshow** from the Objects-Area
2. Choose the Images via de **Load Files**-Button (You can select multiple images at once)
3. Choose a *heading text* for your images to be shown (optional)
4. With the arrow buttons underneath the images you can change the order of appearance.


![](img/editor/1000000000000502000002DC7EB19E47D26CAB2D.jpg)

![](img/editor/addslide.jpg)

To test your slideshow click on the **eye-symbol** of the image-object in the *Created-Objects*-area.
You can move between slides with the **Arrow-keys** on your keyboard.
To deactivate the slideshow you can use the **ESC-Button** on your keyboard.

Repeat this steps with as many slideshows as you like.

***Note:** To change the Behaviour of the media viewer (e.g. the target monitor, scaling, background, etc. ) go to the media settings in the main menu (see more in the chapter media settings)


### Media settings

***Note:** Changes that are made here affect all your media files within the chosen project.

To find the media settings, select *Tools \> Settings*

![](img/editor/1000000000000502000002DCCEBE6408AC240CB2.jpg)

![](img/editor/1000000000000502000002DCEE023E442F7386CE.jpg)


## Test your edddison presentation (browser plugin)

To test your application via your internet browser:

1. In the edddison editor click the **Remote Control button** in the left upper corner.  (See Image)
2. Click **Open in browser** on the right corner of the remote control popup window. (See Image)
3. *Navigate* through your model.

![](img/controller/remote_button.png)
*Step 1*

![](img/controller/10000000000001FF000001706175C889DAA6F302.jpg)
*Step 2*

![](img/editor/1000000000000592000002F0F8A4ED24CD0F20EE.jpg)

For instructions on how to use the controller please see the tutorial **Controller -> App Features**

## Recomendations for using edddison editor

Use two screens. One for the 3D application and one for edddison.

![](img/editor/1000000000000500000002D0FC1D21D2C17F03B7.jpg)




## Software-specific options/restrictions

### tecnomatix plant simulation

* **Start / Stop simulation**

To add a slideshow of multiple images to your presentation:

1. Click **Add Simulation** from the Objects-Area
2. Select the simulation from the dropdown menu
3. Name your Simulation


* Bounding-boxes (Navigation area) are limited to ONE
* No tilt or zoom with POV control


## More Information

If you need more information about the settings and the possibilities you have in the edddison editor, there is another tutorial waiting for you on our homepage: [edddison.com/tutorials/](https://www.edddison.com/tutorials/)


![](img/editor/1000000000000502000002DCC08E4DBB5EC2493F.jpg)


## What next?

**You sucessfully finished setting up your edddison project!**

Now you can enjoy your presentation using one of the many possible presentation styles!

* edddison controller apps (See Tutorial *Controller*)
* edddison browser plugin (See Tutorial *Controller*)
* edddison Mixed-Reality-Interface (Hardware)  (See Tutorial *Hardware*)
* edddison on HP Sprout  (See Tutorial *edddison on HP Sprout*)
