<p style="font-size: 2em;font-weight: bold;">
   Autodesk Navisworks
</p>

![](../img/Navisworks/1000000000000714000004322010CAEA86806634.jpg)

*What you need*

* Autodesk Navisworks Installation (Simulate or Manage 2015 or higher)
* Navisworks Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* Edddison Navisworks plugin - Installs automatically with the edddison software

### 1. Open your Navisworks project

Open a new or existing Navisworks project, in which you would like to use edddison.

### 2. Install and configure edddison plugin

#### 2.1 Download plugin

The Navisworks add-in is installed automatically during the setup process of the edddison software.

### 2. Demo project

To see how edddison works, use one of the demo projects from our [website](https://www.edddison.com/downloads-demo-2/).

### 3. Configure the edddison plug-in

#### 3.1 Start edddison editor

Go to the edddison tab and press Connect.
edddison will be started automatically.

![](../img/navisworks/1000000000000690000003F2D0221C0CB2E65330.jpg)

Lets create our first floorplan!

### 3. Configuring your scene

For edddison to work you need the following:

* Define the walk-able areas in your project with bounding boxes and floor plan images.
* Optional: Define 3D objects

#### 3.1 Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will create **bounding boxes** and **floor plans**.

##### 3.1.1 Set up the bounding box

###### 3.1.1.1 Create new bounding box

![](../img/navisworks/1000000000000690000003F019AC344429D50091.jpg)

* In the edddison-toolbar (ribbon) click **Manage Bounding Boxes** 
* *Create a new bounding box*: Click the **+** button. After choosing a name, a new bounding box will appear at point 0,0,0 in the scene.
* *Edit* the position, size, rotation and the pivot point of your new bounding box.

![](../img/navisworks/10000000000001DE000001B346C2F16D6DF31294.jpg)

###### 3.1.1.2 Create with a selection of objects

You can also create a bounding box using a selection of objects. The bounding box will then be created around your selected geometry.

* Select the objects you want to be inside the bounding box.
* Click the button underneath the **+** (cube in brackets).
* *Edit* the position, size, rotation and the pivot point of your new bounding box.

##### 3.1.1 Create floor plans

A floor plan (2D top view) is needed for easy and precise orientation in the 3D view.

* Select the top view of your project and focus on the bounding box.
* Create an image (.jpg, .jpeg or .png) and use it as a floor plan. 
You can do this with a screen-capture software like the in-built *Snipping Tool* of Windows. To learn more about the Snipping tool [click here](<https://support.microsoft.com/en-us/help/13776/windows-10-use-snipping-tool-to-capture-screenshots>)

***Note:** The image has to have exactly the same size as the corresponding bounding box.*

![](../img/navisworks/10000000000002FD00000242E856E79463C247E2.jpg)


#### 4. Manage 3D objects

A 3D object linked with edddison can:

* be moved around in edddison
* switched on or off 
* be a target for the *POV*-Camera.

All Navisworks objects can be used as *movable* or *stationary* objects.

##### 3.4.1 Link the edddison 3D object

1. Create a new folder in the *Sets -Section* and name it *edddison*.
2. **Select the object** directly in the scene or in the *Selection tree* and **drag it** into the *edddison folder*.
3. Name the object

***Note:** The folder **has to be named**  edddison in order for the object to work.*

![](../img/navisworks/1000000000000696000003F2569A27B91B8F0010.jpg)

##### 3.4.2 Synchronize 3D objects

After creating all your 3D objects, you have to *synchronize* them with *edddison*. 

* **Click Synchronize 3D objects**

***Note:** 3D objects have to be synchronized every time you change them.*

![](../img/navisworks/1000000000000690000003F09C4F2AB41AEF1516.jpg)

### 4. Run project

To test your project click the red *edddison-button* in the *edddison toolbar*. 

You will see the **edddison editor**-software starting in a separate window.

**Please continue to set up your project** in the  *edddison editor*-software. You can find the tutorial on how to do that **here**

***Troubleshooting:**

* If you dont see the edddison editor opening, see if you installed the Software "edddison editor for unity" correctly.  [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)



