<p style="font-size: 2em;font-weight: bold;">
    Sketchup
</p>

*What you need*

* Sketchup Installation (Make or Pro 2014 or higher)
* Sketchup Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* edddison sketchup plugin - [Extension Warehouse](<https://extensions.sketchup.com/en/content/edddison>)

## 1. Open your Sketchup project

Open a new or existing Sketchup project, in which you would like to use edddison.

***Note:** If you don´t want to follow the Tutorial and dive directly into the demo project you can download it on our [website](https://www.edddison.com/downloads-demo-2/)*

## 2. Install and configure edddison plugin

1. Open the Extension Warehouse by clicking *Window \> Extension Warehouse*
2. After signing in, type edddison into the search bar.
3. Afterwards simply click install.
4. When asked if you really want to install this extension, click yes. 

edddison will now be installed inside SketchUp and can be used immediately.

![](../img/sketchup/10000000000003E3000002BB02F77E4173646D86.jpg)

![](../img/sketchup/10000000000003E3000002BBBFE2ABB9308237C9.jpg)

![](../img/sketchup/10000000000003E3000002BBCD2FD4BF22699783.jpg)

You will find the edddison toolbar in sketchup as soon as you closed the Extension Warehouse.

***Note:** If you can' t find the edddison toolbar immediately, it might be behind the warehouse window.*

![](../img/sketchup/1000000000000690000003F202A5CFDA52E50CDB.jpg)

Lets create our first floorplan!

### 3. Configuring your scene

For edddison to work you need the following:

- Define the walk-able areas in your project with bounding boxes and floor plan images.
- Optional: Define 3D objects


#### 3.1. Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will create bounding boxes and floor plans.

###### 3.1.1 Set up the bounding box

There are two ways of creating bounding boxes.

####### 3.1.1.1.  Option 1 - Define size manually

* Select the **create boundingbox** button in the edddison toolbar.

![](../img/sketchup/10000000000001EA00000144FD16F7169D63167A.jpg)

* Set the width, depth and height of the bounding box you want to create and define a unique name.

Clicking the **OK** button creates a new *SketchUp object*-bounding box, which can be moved and sized like other SketchUp objects.

Set its position and scale so it fits to your scene/walk-able area.

####### 3.1.1.2.  Option 2 - Define by selecting existing object

If rooms already exist, you can select the walls or floors of the room or area and edddison will then draw a bounding box from this information.
The easier the geometry (e.g. a cube), the better the result.

* Select all the objects that should be included in the calculation. Then right click on the selection.
* In the context menu, select *Create Boundingbox*

![](../img/sketchup/1000000000000690000003F64F5BB34BB44AEAA6.jpg)

***Note:** Once you have finished creating the bounding boxes, you can
hide them by **unticking** the **Boundingboxes** layer.*

![](../img/sketchup/10000000000001EA00000144BB3E5F8D76DAF315.jpg)

###### 3.1.1 Create a Floor Plan

A floor plan (2D top view) is needed for easy and precise orientation in the 3D view.

* Create an image (.jpg, .jpeg or .png) and use it as a floor plan. You can do this with a screen-capture software like the in-built Snipping Tool of Windows. To learn more about the Snipping tool click here

***Note:** The image has to have exactly the same size as the corresponding bounding box.*

In Sketchup:
1. Click in the menu "View ->Parallel Projection"
2. Click in the menu "View -> Views" and click "Top"
3. With the Mouse wheel and the Pan-Tool zoom and move the camera until your bounding box fills almost the whole screen.
4. Use the Snipping tool to make a screenshot.

### 3.2. Manage 3D objects

A 3D object linked with edddison can:

* be moved around in edddison
* switched on or off 
* be a target for the *POV*-Camera.

All Sketchup Objects (Groups/Components) can be used as **movable** or **stationary** objects.

##### 3.2.1 Link the edddison 3D object

1. Select the *Sketchup Object* you want to convert.
2. Right-Click to open the context menu
3. Select  **Create 3D object**
4. ???

***Note:** Objects must be grouped or components to be used as model objects.*

![](../img/sketchup/1000000000000690000003F64465FFA87B42A7B6.png)

### 4. Run project

To test your project click the red *edddison-button* in the *edddison toolbar*. 

You will see the **edddison editor**-software starting in a separate window.

Please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**

***Troubleshooting:**

* If you dont see the edddison editor opening, see if you installed the Software "edddison editor for unity" correctly.  [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)