<p style="font-size: 2em;font-weight: bold;">
    Unity3D
</p>

*What you need*

* Unity Installation
* Unity 3D Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* Edddison unity plugin - [Asset Store](<https://assetstore.unity.com/packages/tools/input-management/walk-through-controller-73575>)

### 1. Open your Unity project

Open a new or existing Unity project, in which you would like to use edddison.


### 2. Install and configure edddison plugin

#### 2.1 Download plugin

Download the edddison plug-in from the Unity Asset Store: [Asset Store](<https://assetstore.unity.com/packages/tools/input-management/walk-through-controller-73575>)


![](../img/unity/2_1.jpg)

#### 2.2 Import plugin

Then import the edddison plugin into your project.

In the import dialog you will find the edddison plugin, as well as demo files.

* If you want to install the full demo be sure that all of the content is clicked.

***Note:** With this you will import a fully working scene named "edddison_demo_scene" with which you can directly jump to running the project in game mode (Step 4).
If you want to follow the tutorial with the demo project, open the scene "edddison_demo_scene" and delete the object "edddison-object". Continue with Step 2.3*


![](../img/unity/2_2.jpg)

* Otherwise deactivate all content except *edddison-plugin*

![](../img\unity\2_3.png)

Afterwards click *import*

#### 2.3 Add edddison object

To correctly install edddison into your project you have to create an *edddison object*, which includes the "edddison framwork script".

You can do this by going to *GameObject > edddison > create edddison Object*.

![](../img/unity/2_4.jpg)

Select the edddison object within the Inspector.
This object works as a retainer for all edddison functions.
From here you can add cameras, bounding boxes and 3D objects which will be used by edddison.
![](../img/unity/2_5.jpg )

Lets create our first floorplan!

### 3. Configuring your scene

For edddison to work you need the following:

* Define the walk-able areas in your project with bounding boxes and floor plan images.
* Define a camera
* Optional: Define 3D objects

#### 3.1 Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will create **bounding boxes** and **floor plans**.

##### 3.1.1 Set up the bounding box

1. Create a Cube (*GameObject > 3D object > Cube*)
2. Set its position and scale so it fits to your scene/walk-able area.

![](../img/unity/3_1.jpg) 

3. Select the Cube to open it in the Inspector
4. Disable the cube' s Mesh Renderer and Box Collider. (Disable the checkbox next to the component)

![](../img/unity/3_2.jpg)

5. Select the edddison object to open it in the Inspector. 
6. Click the "New Bounding Box" button
7. Drag your bounding box object into the second slot. 
8. In the first slot, enter the name you want to see later in the edddison editor (software).	
![](../img/unity/3_3.jpg)

*Add more bounding boxes to your scene to define different areas of interest and later switch between them.*

##### 3.1.2 Create a floor plan

A floor plan (2D top view) is needed for easy and precise orientation in the 3D view.

* Create an image (.jpg, .jpeg or .png) and use it as a floor plan. 
You can do this with a screen-capture software like the in-built *Snipping Tool* of Windows. To learn more about the Snipping tool [click here](<https://support.microsoft.com/en-us/help/13776/windows-10-use-snipping-tool-to-capture-screenshots>)

***Note:** The image has to have exactly the same size as the corresponding bounding box.*

![](../img/unity/3_4.jpg)

There are different possibilites to make the floorplan screenshot.

**Option 1: In the Viewport**

The easiest method, but not completeley orthographic.

1. Select the bounding box
2. Press F on your keyboard to focus on the object
3. Click the *Y-Axis* in the Unity Scene Gizmo (see image)
4. Use *Shift+Space* while having the Scene-Viewport active to maximize the it.
5. Optionally you can use the scene camera settings to change the field of view to a very low value. Also set the clipping planes "Far" to a very high number. This helps with the orthographic projection, but possibly will render parts of the scene false. (see image)


![](../img/unity/3_5.jpg)
*Step 3*

![](../img/unity/3_6.png)
*Step 5*


**Option 2: with a camera**

This option is a lot more complex and should only be conisidered by professionals to get a floorplan, that absoluteley matches the game output.

1. Select the bounding box
2. In the inspector activate the mesh renderer for the object.
3. Press F on your keyboard to focus on the object
4. Click the *Y-Axis* in the Unity Scene Gizmo
5. Create a new camera
6. Go to *GameObject > Align View with selected*
7. Open a Game Window Tab
8. Select the camera and choose "orthographic" from the inspector
9. Use the *size*-option to adjust the camera height
10. Maximize the Game-Viewport with *Shift+Space* while having the Game-Viewport active
11. Move the camera with the transform options (X and Z Axis). *It helps to undock the inspector*
12. Make sure that the bounding box is aligned with the top and bottom left corner of the camera view. (See image)
13. Then use the *Viewport Rect* option in the Inspector (Camera) and adjust the W and X value until the viewport is matching with the bounding box.
14. Deactivate the Mesh Renderer of the bounding box
15. Use the Snipping tool to make a screenshot 
16. Delete the Camera when you are done

![](../img/unity/3_7.png)
*Step 4*

![](../img/unity/3_8.png)
*Step 13*

![](../img/unity/3_12.png)
*Step 13*

#### 3.3 Manage cameras

The definition of the camera works the same as the definition of the **bounding boxes**. 

1. Either use an existing camera or add a new camera with *GameObject > Camera*
2. Open the *edddison object* in the inspector and click *New Camera*
3. Drag the camera into the second slot. Again, give a name to your camera.
![](../img/unity/3_9.jpg)

***Note:** You can add as many cameras as you like to e.g. add different camera effects, but typically only one camera is necessary.*

#### 3.4 Manage 3D objects

A 3D object linked with edddison can:

* be moved around in edddison
* switched on or off 
* be a target for the *POV*-Camera.

All Unity game objects can be used as *movable* or *stationary* objects.

***Note:** If you want multiple objects to act as a group (e.g. move and be switched on/off together), you can create an "empty game object" as the parent object and link this parent to edddison.*

##### 3.4.1 Link the edddison 3D object


1. Select the *edddison object* and create a *New Movable or Stationary Object*.
2. Drag the desired object into the second slot. 
3. In the first slot, enter the name you want to see later in the edddison editor (software).	
![](../img/unity/3_10.jpg)

##### 3.4.2 Set height of a 3D object
 
The *pivot* of your movable object will always be moved to the bottom of the bounding box. 
This means, that objects in mid-air will fall down to the ground.

To avoid this for any object that should not be standing on the ground, (for example lights), you have two options.

*Option 1*
Give your object an empty parent object.
2. *GameObject > Create Empty*
3. Move desired mid-air object underneath the empty object (parenting)

*Option 2*

* You can set the pivot of the object directly onto the floor plane.

Now the parent object will be moved to the bottom of the bounding box. 

![](../img/unity/3_11.jpg)


### 4. Run project in Game Mode (currently not supported)

***Note:** This is currently not supported except in version 5.6 (32 bit) or lower. We hope that we can provide this feature again in future versions of unity.

To test your project hit the Play-Button. Unity will now start your game in the game window.

Together with the Unity Project you will see the **edddison editor**-software starting in a separate window

Please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**
![](../img/unity/4_1.jpg)



### 5. Deploy project with edddison

To be able to continue with the Tutorial and use edddison have to first deploy your project via the Unity *build*-command.

* *File> Build Settings > Player Settings > Resolution and Presentation*

The recommended build settings are as follows:

![](../img/unity/5_1_2.jpg)

*Most important for Unity to run properly is to check „Run in Background" and „Visible in Background".*

1. Close the Player Settings
2. Select "Build and Run"
3. Select Output Folder
4. Your Unity Project will start automatically when its done building
5. Together with the Unity Project you will see the **edddison editor**-software starting in a separate window
6. Please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**

***Troubleshooting:**

* If you dont see the edddison editor opening after you started the Unity Project, see if you installed the Software "edddison editor for unity" correctly.  [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)