<p style="font-size: 2em;font-weight: bold;">
    Tecnomatix Plant Simulation (PlantSim)
</p>

*What you need*

* PlantSim Installation (13.2 or above)
* PlantSim Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* edddison PlantSim plugin - ??? 

### 1. Open your PlantSim project

Open a new or existing PlantSim project, in which you would like to use edddison.

In this Tutorial we are going to use the edddison PlantSim [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)

### 2. Load Scene and Start edddison

* Load the example scene „*example\_Plant\_Simulation\_Scene.spp*"

![](../img/plantsim/100000000000065C00000438AAF5D9509070221F.jpg)

#### 2.1 Optional: Import Configuration

*If you dont want to follow the tutorial* and want to directly start the edddison example project follow this instructions.
Otherwise go on and follow the steps in chapter *Configuring your scene*

* Start edddison editor with the *edddison button*

![](../img/plantsim/1000000000000470000000B0D821ADA19216CFFB.jpg)

![](../img/plantsim/10000000000000A1000000BF63C8B075C6C260AA.jpg)

*Add edddison beta license „*edddison\_beta\_license\_plant\_simulation.dat*" if asked.

* Import the example configuration in edddison editor. Click on the **Import** button in the project manager and select the *Siemens -- Tecnomatix Plant Simulation project.edddison* file.

![](../img/plantsim/1000000000000503000002D8117CFDB85D2DCA57.jpg)

* Select the imported configuration and open it by clicking the **Open** button in the project manager.

**Follow the Tutorial "Controllers"** to see how to interact with your PlantSim project.

### 3. Configuring your scene

For edddison to work you need the following:

* Define the walk-able areas in your project with bounding boxes and floor plan images.
* Define a camera
* Optional: Define 3D objects

#### 4.1 Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will create **bounding boxes** and **floor plans**.

##### 4.1.1 Set up the bounding box

Define the navigation area with a bounding box.

* Add a Cuboid into the scene

* Set it's properties like in the image

![](../img/plantsim/100000000000020100000394F30F9B1B6195E35C.jpg)

* You will this yellow area (or another color) in the example scene.

![](../img/plantsim/1000000000000376000002E12D47A10E6FAF07C9.jpg)

* Click on **Define Bounding Box** in the *edddison dropdown* to publish it to the edddison editor.

![](../img/plantsim/10000000000000A3000000C0DF0839579A959C13.jpg)

##### 4.1.2 Optional: Create a floor plan manually

A floor plan (2D top view) is needed for easy and precise orientation in the 3D view.

In PlantSim this very easy and can be done **automatically** in a later step when setting up your project in edddison editor.

If you want to do your own floorplan do the following:

* Create an image (.jpg, .jpeg or .png) and use it as a floor plan. You can do this with a screen-capture software like the in-built Snipping Tool of Windows. To learn more about the Snipping tool click here.

*Note: The image has to have exactly the same size as the corresponding bounding box.*

##### 4.2 Manage 3D objects

A 3D object linked with edddison can:

* be moved around in edddison
* switched on or off
* be a target for the POV-Camera

All Unity game objects can be used as **movable** or **stationary** objects. Which object is which can be decided later in the editor.

First we have to link all the objects to the edddison editor.
For this we have **two possibilites**.

###### 4.2.1 Add via Mouse Selection

* Select all objects you wish to interact with edddison.

![](../img/plantsim/100000000000031F000002BC5B5F9E40105A01AF.jpg)

* Click on **Add Selected Objects** in the *edddison dropdown* to publish it to the edddison editor.

![](../img/plantsim/100000000000009F000000C0E2B354258B01FEEF.jpg)

###### 4.2.1 Add via Attribute

* Select an object you wish to interact with edddison.

* Right click on the object and select "**Edit User-defined Attributes\...**"

![](../img/plantsim/10000000000001320000016CF8B10D283E3F4C14.jpg)

* Set the value of the **EdddisonObject** to true.

![](../img/plantsim/10000000000002030000020F9A5B3A69BEABA047.jpg)

* Do this with all objects you want to control with edddison.

* Click on **Add Marked Objects** in the *edddison dropdown* to publish the objects to the edddison editor.

![](../img/plantsim/100000000000009E000000BF269DF53FFBCF6B44.jpg)

### 5. Run project

To test your project click **Start/Stop edddison** in the *edddison toolbar*.

You will see the **edddison editor**-software starting in a separate window.

Please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**

***Troubleshooting:**

* If you dont see the edddison editor opening, see if you installed the Software "edddison editor for unity" correctly.  [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)