<p style="font-size: 2em;font-weight: bold;">
Autodesk 3ds Max Interactive (formerly "Stingray")
</p>

## Getting Started

*What you need*

* 3ds Max Interactive Installation (2.1 or higher)
* 3ds Max Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* edddison 3ds Max plugin - ??? 

***Note:** After installing the edddison plugin click on the ‚**Open plugin folder**' to open the folder with the plugin script file and the tutorial

## Open your 3ds max project

Open a new or existing 3ds max project, in which you would like to use edddison.

***Note:** If you don´t want to follow the Tutorial and dive directly into the demo project you can download it on our [website](https://www.edddison.com/downloads-demo-2/)*

![](../img/3dsmax/100000000000069E00000415BA0A2B066DA698F2.jpg)


## Configure the edddison plug-in

For edddison to work with 3ds max interactive we have to add the **edddison.lua**-script to the project and call it in the **project.lua**-file

### Edit edddison.lua

* Find the edddison plugin-folder (default *C:\\Users\\your user-name\\Documents\\edddison\\stingray*)
* Copy **edddison.lua** from the edddison plugin folder to the script-folder of your 3dsmax project ( */YOUR\_PROJECT/script/ Lua/*)

### Edit project.lua

Open **project.lua** script-folder of your 3dsmax project ( */YOUR\_PROJECT/script/ Lua/*) and search for this line:

``Function Project.on\_level\_load\_pre\_flow()``

Add the following lines **before** the above line:

``local edddison = require \'script/lua/edddison\``

![](../img/3dsmax/100000000000031B00000401C248431F904A85C8.jpg)

Add the following lines **after** the above line:

``edddison.init(false)``

The code should look like this:

``local edddison = require \'script/lua/edddison\'
Function Project.on\_level\_load\_pre\_flow()
edddison.init(false)``

**Optional:**
If you want edddison to close with your project, search for the
following line:

``function Project.shutdown()``

and add the following line **beneath**:

``edddison.close()``

![](../img/3dsmax/100000000000031B00000401C248431F904A85C8.jpg)

## Configuring your scene

For edddison to work you need the following:

* Define the walk-able areas in your project with bounding boxes and floor plan images.
* Define a camera
* Optional: Define 3D objects

### Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will assign **bounding boxes** and **floor plans**.

#### Set up the bounding box

Define the navigation area with a bounding box.

* Add a Cuboid into the scene

* Set it's properties like in the image
* 


You can use the edddison module to subscribe units by name:

``edddison.subscribeUnit (\'YOUR\_UNIT\_NAME\')``

A full example could look like this:

``function Project.on\_level\_load\_pre\_flow()

edddison.init(false)

edddison.subscribeUnit(\'bb\_down floor\')

edddison.subscribeUnit(\'bb\_entire range\')

end``

## Manage 3D objects

The same approach is going to be used for movable 3D objects. Add an object to your Stingray Project, name it and paste it's name in a new **edddison.subscribeUnit**-line in the **project.lua** file.

![](../img/3dsmax/10000000000001EC00000249C2765F8D4B2A39B2.jpg)

![](../img/3dsmax/100000000000069E0000086F003842EB7E375D4D.jpg)

##  Run project with edddison

Save your **lua-files** and press the "**Run Project**" button in Stingray to run the scene and start edddison.

At the first start of edddison, don't forget to add your edddison license-file.

![](../img/3dsmax/100000000000058800000323B0855B820B3A2001.jpg)

## Deploy project with edddison

If you create an executable file out of your project, you can use it on any machine where edddison is installed. Then edddison will start automatically at opening the "**.exe**".