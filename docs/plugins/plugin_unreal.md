<p style="font-size: 2em;font-weight: bold;">
    Unreal Engine
</p>



## Getting Started

*What you need*

* Unreal Engine Installation
* Unreal 3D Project [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* edddison unreal plugin - [UE4 Marketplace](<https://www.unrealengine.com/marketplace/en-US/slug/edddison-create-interactive-3d-presentations/reviews>)

## Install edddison plugin

You can install the edddison plugin from the UE4 Marketplace [UE4 Marketplace](<https://www.unrealengine.com/marketplace/en-US/slug/edddison-create-interactive-3d-presentations/reviews>)

![](../img/unreal/ue4marketplace.png)

## Create a project in Unreal Editor

Open a new blank or an existing Unreal project, in which you would like to use edddison.

***Note:** If you don´t want to follow the Tutorial and dive directly into the demo project you can download it on our [website](https://www.edddison.com/downloads-demo-2/)*

## Activate and configure edddison plugin

### Activate the edddison plugin

Enable the edddison plugin in the Plugins window of the Unreal Engine. 
***Note:** You will maybe asked to restart Unity to enable the plugin. Do this by clicking "Restart now".*

![](../img/unreal/plugintab.png)

![](../img/unreal/enableplugin.png)

***Troubleshoot:** If at the restart of Unreal a message pops up saying "Disable ThrottleCPUWhenNotInForeground", just restart Unreal one time again manually or change to displayed option in the Unreal Settings. See Chapter 3.3. for this.* 

Switch to the edddison tab in the Modes window.

![](../img/unreal/modes_edddison.png)



## Configure the edddison plug-in

This plugin-page works as a retainer for all edddison functions.
From here you can add cameras, bounding boxes and 3D objects which will be used by edddison.

The first section consists of the following general options:

**Autostart with editor**: Start edddison when Unreal editor starts. (If plugin is activated)

**Autostart with game**: Start edddison when Unreal game starts.

**Enable Realtime in Viewport**:  Equivalent to activating "Realtime"-View in Viewport Options.

**Start edddison**, **Stop edddison**, **Update edddison**, **Clear edddison settings** (These options are covered in the chapter "Test edddison")


![](../img/unreal/100000000000026400000376D5CEA5C955CD7D11.png)

![](../img/unreal/100000000000014C0000020F54E6169B754C037A.jpg)


### Configure Unreal Engine to interact with edddison in real-time (important)

![](../img/unreal/1000000000000218000000F3962C9DE01E1EC3AD.jpg)

In the Unreal Editor Top Menu:  *Edit > Editor Preferences > Performance > **uncheck** "Use Less CPU when in Background"*.

![](../img/unreal/10000000000002940000012A2FC28155E43AA7FD.jpg)

Afterwards click *Set as Default* to keep the setting for all your future projects.

***Note**: If you do not **uncheck** this, a fluent camera control with edddison in editor mode is blocked by the Unreal Engine. This setting does not affect the game mode.*

The last option you should check is the "Realtime Viewport".
In the Viewport click the arrow on the top left corner and **check 'Realtime'**
This allows you to use the edddison camera to interact in the editor viewport.


![](../img/unreal/realtime.png)

Perfect! Now lets create our first floorplan!

## Configuring your scene

For edddison to work you need the following:

* Define the walk-able areas in your project with bounding boxes and floor plan images.
* Define a camera
* Optional: Define 3D objects


### Manage floor plans - (walk-able areas)

To define the walk-able areas in your project, we will create **bounding boxes** and **floor plans**.

#### Set up the bounding box

1. Place a Cube (*Modes > Place > Basic > Cube -> Drag to scene*) to create a bounding box (See Image). *Rename if needed*

2. Define its size and orientation.	 (*see 4.1.2. for more details*)

3. Disable collision by setting **NoCollision** in the **Collision**-Tab of the objects details. (See Image)

4. Set the cube invisible by **unchecking** *Visible.* in the **Rendering**-Tab of the objects details. (See Image)

5. Select the cube in the **outliner**.
6. Select the edddison plugin in the **Modes**-Window.
7. Click on the plus icon in the **Bounding boxes**-Section to add it to the edddison plug-in. (See Image)

![](../img/unreal/staticmeshactor.png)
*Step 1*

![](../img/unreal/nocollision.png)
*Step 3*

![](../img/unreal/visible.png)
*Step 4*

![](../img/unreal/createbb.png)
*Step 7*

The Cube (*Static Mesh Actor*) should now be visible in the section **Bounding Boxes** like in this image:

![](../img/unreal/bb.png)


#### Position the bounding box

edddison calculates its camera position and movement from the currently active bounding box. Which means it can only move inside this box.

You can position and rotate the bounding box as you like.

To resize and position your bounding box to fit as best as possible to your desired walkable model area, do the following:

**While having the bounding box selected:**

Switch to **local (object) space** in your viewport.

![](../img/unreal/cyclelocal.png)

![](../img/unreal/cyclelocal2.png)

Switch to **top view** and **wireframe** in your viewport options.

![](../img/unreal/topwire.png)

Use **F** on your keyboard to focus the camera on the bounding box.

**In your viewport:**

The **red arrow** of the objects gizmo always points in the vertical dimension (up, north) of the edddison 2D floorplan (in the edddison editor) . (See Images)

![](../img/unreal/100000000000066C0000043850D335F6B322B06F.jpg)

*Bounding Box in Unreal Editor*

![](../img/unreal/1000020100000502000002DCD018A6D455E7B173.png)
*Bounding Box(Floorplan) in edddison editor*


**Make sure, that the red arrow is pointing up (north) by rotating your bounding box accordingly**

This will be the "shorter side" of your bounding boxes floorplan.

The **green arrow** always points in the horizontal dimension of the edddison 2D floorplan (in the edddison editor). (See Images)

This will be the "longer side" of your bounding boxes floorplan.

**Resize your bounding box according to your walk-able area in your 3D Model and make sure you got the red arrow pointing "north" and the green arrow pointing "east"**


#### Create a floor plan

A floor plan (2D top view) is needed for easy and precise orientation in the 3D view.

* Create an image (.jpg, .jpeg or .png) and use it as a floor plan. 

In Unreal Editor this is really easy:

1. Switch to **local (object) space** (see chapter "position bounding box")

2. Use **Top view**, select the bounding box, make it visible and focus on it.

3. Set all things invisible which you dont wish to see on your floorplan. (*Object Details > Rendering > Visible "uncheck"*)

4. Open the **High resolution screenshot** tool from the arrow on the top left menu in your viewport. (See Image)

5. Span the capture rectangle exactly on the cube.

7. Set the bounding box **invisible** (*Select Bounding Box Object > Object Details > Rendering > Visible "uncheck"*) .

8. Activate one of the possible *viewport visibility* options from the menu on the top left in your viewport. We suggest "Lit". (See Image)

9. Make the screenshot by clicking the *Save*-Icon. On the bottom a popup will show you where the screenshot has been saved. Click it to open the File Explorer at that location.(See Image)

If you are statisfied with the image move on, otherwise repeat steps.

![](../img/unreal/screenshot.jpg)
*Step 4*

![](../img/unreal/floorplan.png)
*Step 8*

![](../img/unreal/savescreenshot.png)
*Step 9*

### Manage cameras

#### Create Camera (mandatory)

The definition of the camera works the same as the definition of the **bounding boxes**. 

1. Place a **CameraActor** (*Modes > Place > All Classes > Camera -> Drag to scene*)  to define the camera for edddison. (See Image). *Rename if needed*. 
3. The camera should always be **Movable** to prevent any failure in **Packaging**. You can set this in the cameras details. (See Image)
4. Select the cube in the **outliner**.
5. Select the edddison plugin in the **Modes**-Window.
6. Click on the plus icon in the **Cameras**-Section to add it to the edddison plug-in. (See Image)

![](../img/unreal/cameraactor.png)
*Step 1*

![](../img/unreal/cameramovable.png)
*Step 2*

![](../img/unreal/setcamera.png)
*Step 6*

The **CameraActor** should now be visible in the section **Cameras** like in this image:

![](../img/unreal/cameraactor2.png)

***Note:** You can add as many cameras as you like to e.g. add different camera effects, but typically only one camera is necessary.*


##### Set initial camera position (optional)

edddison sets the cameras position right after activating the edddison camera. 
To define the cameras position before edddison is active use a **Player Start** object.

Without this option, the camera starts at world origin (0,0,0).

1. Add a **Player Start** at the intended position in scene. (If there is not already a **PlayerStart** placed in the Scene.) (See Image)
2. Open Window *World Settings* (*Window > "check" World Settings*) (See Image)
3. Set the **GameMode\_edddison\_Base** as GameMode of your project. (See Image)

![](../img/unreal/10000000000006080000043804C030B52CBFB466.jpg)
*Step 1*

![](../img/unreal/world settigns.png)
*Step 2*

![](../img/unreal/gamemodeoverride.png)
*Step 3*

***Note**: The GameMode asset is in the  **EdddisonPlugin Content → Examples** folder of the EdddisonPlugin.*

![](../img/unreal/10000000000003750000012C873851F7746B7690.jpg)

### Manage 3D objects (optional)

A 3D object linked with edddison can:

* be moved around in edddison
* switched on or off 
* be a target for the *POV*-Camera.

All Unreal game objects (*Actors*) can be used as *movable* or *stationary* objects.

***Note:** If you want multiple objects to act as a group (e.g. move and be switched on/off together), you can create an actor as the parent object and link this parent to edddison.*

#### Link the edddison 3D object

1. Create a new Object (*Actor*) or select an existing object. 
2. Select the edddison plugin in the **Modes**-Window.
3. Click on the plus icon in the **Objects**-Section to add it to the edddison plug-in. (See Image)


![](../img/unreal/10000000000004410000037465D6D5B96ADF5EBC.jpg)

## Run/Deploy project with edddison

### in Play Mode

You can test your projet directly in the Unreal Editor:

1. Hit the Play-Button of the Unreal Editor.
2. Hit the Play Button in the edddison plugin (*Modes > edddison plugin > Play Button*)

Unreal will now start your game in the Viewport and at the same time you will see the **edddison editor**-software starting in a separate window.

Please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**

You can always stop the edddison editor interaction with the Unreal model by clicking the Stop-Button (*Modes > edddison plugin > Stop Button*).

Other options are: 
**Update edddison**: If you made changes to the model while the edddison editor was active you need to click here to apply those changes.
**Clear**: Clear all edddison plugin configuration.

![](../img/unreal/playunreal.png)

![](../img/unreal/play1.png)

### Switch to the edddison camera view

To see and use the edddison interaction in the Unreal editor view, you have to switch to the view of the edddison camera.

Click on the view selector button and select the camera form the list.

![](../img/unreal/1000000000000108000001697285E4549B6C5AC2.jpg)

Move the camera with edddison.


### Build & Deploy Project

To configure your presentation, please continue to set up your project in the  **edddison editor**-software. You can find the tutorial on how to do that **here**

1. Start the game via the .exe or the "Launch"-Option in the Unreal Engine. It launches edddison automatically in a separate window.
2. **Import** and load the edddison configuration (\*.edddison).
4. Work with edddison.
5. Closing the game closes edddison too.

If you packed your game and want to use it:

* as a **stand-alone .exe**: Just build and package your project like any other unreal game. Be sure to have to option *Autostart with game* in the edddison plugin as well as the plugin itself activated before packaging.
* on another computer: Either copy your Unreal project or build a stand-alone game and transfer it to the other computer. You will have to install edddison editor and an edddison license on the other computer.
* **Export** the edddison configuration (\*.edddison) and ship it with the game files, if you want to use the game with edddison on another computer.

## Use Blueprint functions to start/stop edddison (optional)

To start edddison in a game manually:

Uncheck *Autostart with game* in the edddison plugin options.

***Note:** An enabled autostart option overrides the Blueprint functions.*

Use the edddison Blueprint functions.

![](../img/unreal/10000000000003750000012C873851F7746B7690.jpg)

![](../img/unreal/100000000000028C00000132A7703E36442B0C3D.jpg)


## Use a mouse input to start/stop edddison manually (optional)

**How it works:** Have a look into the **UI\_edddison\_Mouse\_Input** widget.

![](../img/unreal/100000000000019A00000070CFB39329959F0175.jpg)

You can display two buttons in the viewport to start and stop edddison in your game.

![](../img/unreal/1000000000000190000000F4D7C65BE673383F84.jpg)

Use the **UI\_edddison\_Mouse\_Input**.

To show the widget drop the **BP\_edddison\_Mouse\_Input\_Pawn** widget into your scene.

![](../img/unreal/10000000000002A20000010D3802DF0E9D96AEFB.jpg)

In the World Settings of the Game set *GameMode\_edddison\_Mouse\_Input* to override the Game Mode.

## View in VR (optional)

With edddison you can walk through the scene in VR without adding complex modifications for VR navigation.

You can move the camera in the edddison editor while another person watches through a VR device (**HMD**). If the VR device supports head tracking (position, orientation), the VR user can control the view additionally.

### Setup for VR device

1. Use the same camera as added to the edddison plugin. No special VR camera is necessary.
2. **Uncheck** the camera setting **Use Pawn Control Rotation.**
3. **Check** the camera setting **Lock to HMD.**  

![](../img/unreal/1000000000000184000000F81C4FBB862725C3D5.jpg)


### Blueprints to enable/disable stereo view (optional)

Create this blueprint for the camera (**CameraActor**) to switch VR/stereo mode online.

![](../img/unreal/100000000000017300000132CD0D38771834D99F.jpg)

Press '**n**' key to enable stereo

Press '**m**' key to disable stereo

### Enable VR in a packaged project (optional)

If you have not integrated the blueprints into your project, you can still activate the VR view. Start your packed project with the command line option **-vr**.