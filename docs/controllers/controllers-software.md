# Software Controllers

## Test

### 1. Add Device

Add and configure the device in edddison.

1. Click **Add Device** in the left upper corner of the editor.
2. Choose **Tablet** as your device.
3. Click **Finish**
4. You should now see the *Tablet* Device next to the *Add Device*-button

![](../img/controller/adddevice.jpg)
*Step1*

![](../img/controller/1000000000000502000002DC6FAD6AE4E4CC72A9.jpg)
*Step2*

![](../img/controller/adddevicedone.png)
*Step3*

Keep the edddison editor open and follow the tutorial to download one of our controllers (or use your internet browser).


### 2. Download App

#### 2.1. Android

Download the edddison app from the [ Google Play Store](<https://play.google.com/store/apps/details?id=com.edddison.controller>)

![](../img/controller/playstore.jpg)


#### 2.2. iOS App Store

Download the edddison app from iTunes App Store.

 ![](../img/controller/100000000000078000000410E24C489C2C2E1573.jpg)


### 3. Connect App to edddison editor

* Make sure that your **device is connect to the same WiFi network as your PC** running the edddison editor.
* Make sure *edddison editor* is running on your pc ( See Chapter 1 )
* **Start the edddison app** from your device
* The application will *automatically connect* with the edddison editor on your PC
* **Select** your edddison project

![](../img/controller/1000000000000A00000006409565EB49D2B5621B.jpg)

***Troubleshooting**: If you can´t see any edddison projects in the app please make sure that you are connected to the same WiFi network as the PC and that there are no firewalls or other barriers activated.

### 4. Use a browser on any (touch) device

Alternatively to the controller apps (iOS and Android) you can also use any *touch screen device* as a remote control device. 
*Alternatively* you can also use the browser without touch-functions by *navigating with the mouse*.

The application runs on every touch device and any OS, that has the following internet browsers: Firefox, Chrome, Edge (see Note) ).

***Note:** When you open up Microsoft Edge, in the search tab („Search or enter web address“) write „about:flags“ and click enter. Then scroll to the section called „Standards Preview“ and search for the menu bar called „Enable touch events“. If the value within is set to „Always off“, change it into „Only on when a touchscreen is detected“. After restarting Microsoft Edge, touchscreen events for controller should work normal.*

You have **two options** how to connect to your edddison project.

**Option 1: Connection via WiFi**

**Connect** your device via *WiFi* or *network cable*. The *touch screen device* has to be in the same network as your computer.

**Option 2: Connection via second monitor**

Plug your *touch screen device* as second monitor to your computer (for example via an HDMI cable).

**Afterwards follow the instructions below:**

1. In the edddison editor click the **Remote Control button** in the left upper corner.  (See Image)
2. **Open your internet browser** on the *touch screen device* and type in the URL visible in the edddison editor. (See Image)
3. *Navigate* through your model.
4. **Optional:** To use the full screen of your device, set your browser to *Full screen* (F11 in most browsers) to enjoy the same appearance and functionality as with the native app.

![](../img/controller/remote_button.png)
*Step 1*

![](../img/controller/10000000000001FF000001706175C889DAA6F302.jpg)
*Step 2*

![](../img/controller/100000000000028D0000019CF16F931BD0A4CB98.jpg)
*Step 2*

### 5 How to use

Now you can control your interactive 3D application via all your connected devices.

***Note**: You can connect multiple controllers to interact simultaneously and in real-time. See a Tech Demo [here](<https://www.youtube.com/watch?v=qsbxTMFE4ek>)*

To see a video on how to use the controller you can also see a short video [here](<https://www.youtube.com/watch?v=AbeDJGKQWuI&list=PLqCpnh2wkMWcsrkfiMl2npGcqyOTqgRzG&index=3>)

#### 5.1. Navigate and control the interactive 3D application

You can navigate in your 3D Model by:

* **Move:** Touch the *POV-icon* with your finger and drag it over the floor plan to move in 3D Model.
* **Rotate:** *Hold the POV-icon* with one finger (e.g. index finger) and *touch the floor plan with your other finger (e.g. thumb)*. Then *slide your other finger* (e.g. thumb) in a circle around the *POV-icon.* Alternativley you can also use the *Rotate-Button* on the right side of the browser addon
 
You will see the changes in real-time in your 3D-Model
 
 Do the same with any movable object in the floorplan. 
 
 ![](../img/controller/floorplan.png)


#### 5.2. App Features - Left Toolbar

**Open the edddison toolbar** with the *arrow on the right* to change settings, floor plans, 3D objects or turn on and off pictures, videos and slideshows.

#####  5.2.1. Settings

Use the tab *Settings* to:

* **Change to Fullscreen** (Alternatively activate Fullscreen in your browser)
* **Adjust Brightness**
* **POV Navigation**: Switch between *Follow my Finger* and *Multi-Touch rotation*
* **Toggle Toolbar - Overlap/Fixed**
* **Adjust Opacity of the Toolbar**
* **Zoom Modes**: Switch between *Off*/*Pan manually*/*Pan automatically*/*Pan on border* 

![](../img/controller/settingsneu.png)

#####  5.2.1.1 POV Navigation types

**Multi-Touch rotation** (Default): Use your Index finger holding the POV-Icon to move and your Thumb to rotate.
**Follow my Finger**: Use your finger to hold and drag the POV-Icon around the scene. The camera will rotate automatically depending on the moving direction.

#####  5.2.1.2 Zoom Modes

To zoom in and out of your floorplan use two fingers on the floor plan like you would on any other touch device.

**Off**
**Pan manually**:Touch the floor plan without touching one of the POVs or 3D Objects and drag the floorplan to pan manually.
**Pan automatically**: The pan will adjust automatically when moving around the scene.
**Pan on border** : Drag your POV-Icon to the borders of the floor plan to pan by "pushing" the border.

##### 5.2.2. Floor plans

Switch between different floor plans.
The Floorplans are represented by icons with the set thumbnail (in edddison editor).
Activate them by touching the icons.


![](../img/controller/floorplans.png)


##### 5.2.3. Point of view

There are two different POVs in edddison:

* **POV (First Person):** This basically acts as the spectators eyes and allows him to walk through the model in a First-Person-View. It has a given height (eye height).

* **Target Point of View (Look-at-Camera):** This camera acts as an 360°-overview camera with a fixed target. Typically you would use this camera to look at a defined target like a 3D Object and pivot around it. The Yaw will be set automatically.

**Acticate/Deactivate** the normal *POV* and the *Target POV* by touching them.
You can then use the Icons in the floorplan to navigate in the scene.

![](../img/controller/pov.png)


##### 5.2.4. Adjust POV - Right Sidebar
 
On the right side of the controller you can find a toolbar to tune your Point of View.

* **Height:** Change the height of the *POV-eye*
* **Rotation:** Rotate your Point of View (POV)
* **Tilt:** Tilt your Point of View up and down
* **Field of View:** Adjust the Field of View of your POV

***Note**: Shóuld you not be able to see the right sidebar click on the arrow on the right to fold it out.*

***Note**: The Target POV only has the settings Height and Zoom available*

 ![](../img/controller/rightsidebar.png)



##### 5.2.5. Viewpoints

**Viewpoints** give you easy access to focus on predefined areas of the 3D Model.

**Activate** a Viewpoint by touching. 

![](../img/controller/vp.png)

![](../img/controller/vps.jpg)


##### 5.2.5. 3D objects (Movable and Stationary)

Switch between all your created 3D objects.

![](../img/controller/movables.png)
*Tab movable objects*

**Activate** your movable object by touching the corresponding icon.
**Move** your object the same way you move your *POVs*

![](../img/controller/stationary.png)
*Tab stationary objects*

**Activate** your movable object by touching the corresponding icon.
***Note:** Since stationary objects are not movable they will not appear in the floorplan.*

##### 5.2.6. Media viewer

Show single pictures, slideshows or videos using the media tab.

![](../img/controller/media (1).png)

**Activate** the media (image/slideshow/video) by touching the corresponding icon. The selected media will start in fullscreen.

**Deactivate** the media (image/slideshow/video) by clicking on the x in the top right corner.
**Slideshow**: Move between the slides by swiping (touch devices) or using the arrow keys.

##### 5.2.7. Simulations ( only tecnomatix plant simulation)

* **Start / Stop simulation**
* **Reset Simulation**
* **Fast Forward**
