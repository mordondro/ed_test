
#  Projection table

**DESCRIPTION!!!**


![](img/hardware/100000000000133000000AC8B7838F22981955DA.jpg)

![](img/hardware/100000000000133000000AC8AE1732A8A433913C.jpg)
![](img/hardware/100000000000133000000AC8196FC7AF63C211C0.jpg)

 ![](img/hardware/100000000000133000000AC89344860997C78782.jpg)

What you need

* A Projection Table - How to build one **(LINK)**
* The edddison software - [Please login on edddison.com to access this link ](https://edddison.com/index.php/edddison/software/download2/plugin/unity/)
* Edddison plugin for your 3D Software
* Object Markers
 
### 3. Floor plans

Floor plans help you to navigate in the 3D scene. Every floor plan has a unique ID, represented by a marker.

Floor plans will be shown on the edddison projection table via a projector.

### 4. Object marker

 ![](img/hardware/100000000000133000000AC8B7838F22981955DA.jpg)
 
Object markers can be linked with any object in the 3D scene.

They can be used to control the point of view, to show and move additional 3D objects within the scene or to add pictures and videos to
your presentation. They are also used to change between rooms/ floors.

![](img/hardware/10000000000002010000018F747DB65AEB7A9C99.jpg)

### 5. Install edddison tracking

To use the edddison projection table, you have to install the edddison tracking.

![](img/hardware/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)

### 6. Prepare your project

To use edddison to present your project, you first have to prepare it in your 3D software and in the edddison editor.

You can find instructions on how to do this in the tutorials How to design an interactive SketchUp walk through or How to design an interactive Unity 3D walk through, depending on which software you wish to use.

![](img/hardware/1000000000000502000002DCB5010C77D8FD341E.jpg)


### 7. Add license

The edddison tracking license has to be added separately after the installation of the edddison tracking. You can do this via *Help \> Add license*

![](img/hardware/1000000000000502000002DCBE47A4AE47878EC4.jpg)

### 8. Add device in edddison

After your project is prepared in edddison, you can add your projection table. To do this, click on the Add device button.

![](img/hardware/1000000000000502000002DC6FAD6AE4E4CC72A9.jpg)

Select **edddison Projection table** and follow the wizard.

**Note:** Make sure that your camera is connected to your PC before starting the wizard.

![](img/hardware/1000000000000502000002DC991E86009CA38749.jpg)

### 10. Calibrate projection table

The wizard will lead you through the calibration of your projection table. Please follow all the steps carefully. You can find more information about the camera settings in point 14.

![](img/hardware/10000000000005020000037497EDE3FD37F75C00.jpg)

#### 10.1 Check markers

As already explained before, every object needs a marker. You can see the number of the marker of each object when selecting it in edddison. As soon as you add an object to edddison, the next free marker will be assigned to it. If you want to use already printed markers from other projects, you can change the marker numbers on your own.

**Note:** Marker numbers are only visible if a projection table is already added.

![](img/hardware/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)


#### 10.2 Print

You can print all the markers used in your project directly via the print dialogue.

In the wizard you can decide if you want to print Floor plans, Object markers or Calibration markers.

**Note:** Printed floor plans are used only for the edddison tabletop, therefore you will not need this option.

![](img/hardware/100000000000050200000374586981F185CA0B6B.jpg)

#### 10.3 Calibration markers

Calibration markers are used to calibrate your projection table once in the beginning of your presentation. You can either print them while doing the setup of your projection table (see point 8) or with the print dialogue.

![](img/hardware/10000000000005020000037436C8B856393D43BB.jpg)

#### 10.4 Object markers

Select the object markers you want to print. As long as you use the same numbers, you can obviously also reuse markers from other projects.

**Note:** The necessary marker size depends on the quality of the camera you use. The preset marker size is suitable for a standard webcam, if you use a better camera, you can probably also make them smaller.

![](img/hardware/1000000000000400000002ABAD2CE65AE04BF460.jpg)

#### 10.5 Attach markers to tangible objects

Make sure you attach the markers to your figures correctly. The little number on the marker has to face the same direction as the object.

![](img/hardware/100000000000050200000374AD26C21D82CBFAD1.jpg)

#### 10.6 Start device

To start the device, click on Projection table. The inspector on the right side of your screen will show you a few settings and the option to start your device.

![](img/hardware/100000000000050200000374AD26C21D82CBFAD1.jpg)

#### 10.7 Start projection

After starting the device, the camera preview window will open (if you can't see it immediately, it might be behind the edddison window).

Afterwards, you also have to start the floor plan projection. To do so, click on the Start projection GUI button.

![](img/hardware/1000000000000373000002C6E74FD728F1EFDCE0.jpg)

#### 10.8 Set up projection table

Now you can move the projection GUI window to your projection table and maximize it (*file \> maximized*). Pressing Esc ends the full screen mode.

![](img/hardware/100000000000022F0000019504895E9E63DC8AE3.jpg)

#### 10.9 Camera preview

In the camera preview window you can check if all markers are recognized correctly by your camera. If this is not the case, try changing the lightness of your camera or try with bigger markers.

**Note:** If your markers are too small, the camera won't recognize them even if everything else is set correctly.

![](img/hardware/100000000000022F0000019504895E9E63DC8AE3.jpg)

#### 10.10 Correct camera preview

![](img/hardware/10000201000001C2000001C2C2834E14D5C5C90E.png)

The picture on the left shows you the correct recognition of your floor
plan. You will see the green rectangle if the camera can recognize your
floor plan correctly.

As soon as you see the green rectangle, untick Corner calibration active in the edddison editor.

Your projection table is now ready.

![](img/hardware/100000000000021D0000018A74E42BB39D4A1215.jpg)

#### 10.11  Incorrect preview

![](img/hardware/10000201000001C2000001C24B95BE9EF07865BD.png)

If there is too much light, not enough light or the light is not equally distributed on the floor plan, the camera will not recognize all the markers and therefore the projection table will not work correctly. Also, make sure all calibration markers are placed correctly.

### 11. Camera parameters

![](img/hardware/1000000000000226000001A27DEC6FC0F2F3FD9C.jpg)

#### 11.1  Lightness

To see if the brightness is set correctly, place your Point of view (POV) object on the plan and move it around. In the preview window, you should see the ID in orange letters. Your POV should move fluidly in the preview window without lag. If the camera recognition is too slow, the brightness has to be adjusted. Try moving the slider to find the best position, depending on the light conditions. Generally, the preview should be really dark and the plan should be hard to recognize.

![](img/hardware/100000000000050200000374AD26C21D82CBFAD1.jpg)

#### 11.2  Recognition outside

![](img/hardware/100000000000018C00000110A95E305EFFBD001D.jpg)

![](img/hardware/100000000000019400000100FAEEC6B64C4EA5A8.jpg)

**This allows you to change how** far outside the corner markers your object markers will still be recognized. Normally, you can leave this slide on default.

![](img/hardware/100000000000050200000374AD26C21D82CBFAD1.jpg)

#### 11.3 Jitter compensation

**This compensates for small** movements of your camera.
