# Virtual Reality


![](img/vr/1000000000000AE9000005EE1630C697B929932E.png)

Locomotion in VR -- Virtual Reality & edddison
-----------------------------------------------------------------

#### 1. Problem

If there's one problem we're most desperate for VR to solve right now it's locomotion. Popular choices include teleportation, artificial track-pad movement and room-scale. All of these have their advantages and their drawbacks. All solutions need user skills to work with a controller which is tricky for non technical users.

[http://users.rowan.edu/\~conet/rhythms/Resources/Loco.nonloco.definitions.html](<http://users.rowan.edu/~conet/rhythms/Resources/Loco.nonloco.definitions.html>)

#### 2. Our approach

*Assist clients, stakeholders and managers* to orient themselves in 3D models. A *second person (the operator)* guides users through the presentation.

* The **Operator** guides users through the presentation
* **Users** look around via HMD (head-mounted display)
* The target group for this product is people not familiar to use joysticks, hand held or controller

**Operators tasks**

* chooses the floor-plans (Navigation areas).
* move the user through the scene (360 degrees)

**User tasks**

* look around
* Rotation in VR
* look up and down

[**Watch a video**](https://www.youtube.com/watch?v=lndrzeWyRp8)

#### 3. Advantage

* Prevent user disorientation
* Manage the presentation
* Focus on important things -- You are guiding through the presentation

#### 4. Tips 

##### 4.1 for the Operator 

* Do all movements and activities carefully thought out
* Inform the user before jumping to other areas
* Ask users what they want to see
* Use the target POV while changing the floor-plan (The view focus always to the room center or an object)
* Turn the view, do it slow with the tangible control object or on touchscreens use the control menu on the right side.
* If people are sitting. Turn the POV around (360 degrees). So user can look behind.
* Adjust the elevation (POV level)
* Move objects in the scene
* Switch on and off objects
* Activate and show additional content like a video,
    image or slide.

![](img/vr/1000000000000592000002F0C87AE6C3B6EA9C54.jpg)

##### 4.1 for the User

* look around and rotate
* look up and down


#### Technical Setup
------------------------------------------

#### 1. Supported 3D Software

* Unreal (tested)**
* Unity (Not yet, comes with the next edddison
    update)
* Autodesk Stingray (not tested)

#### 2. edddison hardware for operators

* Tablets, Smartphones (Android, Apple, Windows)
* Touchscreens (Windows all OS)
* Mixed reality devices (camera tracked items). Use
    your own Webcam, printed plans and markers to navigate in 3D
    models.
* Sprout by HP. Enable users to interact with physical objects and the touch-mat. Comes with an integrated Windows PC.

#### 3. Windows PCs

* Running edddison and 3D scene

#### 4. HMD

* HTC VIVE

* Oculus

* Windows 10 VR Headsets


### Prepare in Unreal
------------------------------------

![](img/vr/1000020100000184000000F8E5B97CF5BF084801.png)

Unreal    engine 4.15 or younger
Edddison 1.2.6
Oculus Rift



If you use edddison for VR, you can use your existing non VR project. It is not neccessary to add further navigation and trigger planes for interacting with a VR controller.

Just put a simple CameraActor into the scene and add it to edddison. Check the "Lock to Hmd" setting and uncheck the "Use Pawn Control Rotation". Then try out with the VR Preview play mode.

![](img/vr/1000020100000173000001320723434287E4700B.png)

Use blueprints to switch stereo view at the operator monitor:

Create this blueprint for the CameraActor to switch stereo mode online.

* Press 'n' key to enable stereo
* Press 'm' key to disable stereo

* How to prepare in Unity and Autodesk Stingray comes in the next edddison update!
