# Hardware Controllers- edddison tracking system

**To control your 3D software with tangible objects, you can** use the **edddison projection table** or **edddison tabletop** in combination with tangible objects.

An **edddison marker** is a printed label (like a barcode) that is attached on a control object. The markers are read by a camera. Adding markers to any tangible object generates a control object that can be used in edddison. These real figures can be produced with a 3D printer, a laser cutter or by adding markers to existing figures (e.g. toys).

By moving these tangible objects on your floor plan, you can control objects within the 3D scene.

That' s how easy it is to control your 3D scene with edddison.

**IMAGE**

There are 3 different ...




#### 8. Object markers


Select the object markers you want to print. As long as you use the same numbers, you can obviously also reuse markers from other projects.

***Note:*** *The necessary marker size depends on the quality of the camera you use. The preset marker size is suitable for astandard webcam, if you use a better camera, you can probably also make them smaller.**

![](img/hardware/100000000000144000000D800F7091518D18AB7D.jpg)

#### 9. Attach markers to tangible objects

Make sure you attach the markers to your figures correctly. The little number on the marker has to face the samedirection as the object.

#### 4. Object marker

* Object markers can be linked with any object in the 3D scene.

* They can be used to control the point of view, to show and move additional 3D objects within the

* Scene or to add pictures and videos to your presentation.

#### 5. Install edddison tracking

![](img/hardware/10000000000002010000018F747DB65AEB7A9C99.jpg)

To use the edddison for your photobook generation you have to install the edddison tracking software.

![](img/hardware/bild1.jpg)