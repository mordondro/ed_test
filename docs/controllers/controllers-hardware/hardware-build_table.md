# Build your own edddison projection table

**How to build your own edddison projection table**

![](img/hardware/1000000000000AC80000102C92381BDB792F7497.jpg)

### 1. Setup

### 2. What you need


* **edddison projection kit**
* **projector**
* **housing with glass slab**

![](img/hardware/10000000000002CD000001C2ACC5E58A45E8748D.jpg)

#### 2.1 edddison projection kit

**The edddison projection kit includes:**

* **camera**
* **camera bracket**
* **rear projection film**
* **LED IR stripes and brackets**
* **basic control object (little eddd)**
* **USB cable**
* **chassis connectors**
* **marker stickers**

![](img/hardware/100000000000040000000280A73AE8D0A77FE3E5.jpg)

![](img/hardware/1000000000000400000002807E3A663F16A56D3C.jpg)

####  2.2 Projector

You can use your favorite projector for your projection table, but there are still a few things to consider when deciding on a projector. Make sure your projector is able to focus on the projection screen. The focus distance is measured from the lens to the projection screen. If you include one or more mirrors, this distance becomes bigger. Furthermore, makes sure the size of the projection fits the size of your glass slab. Depending on the size of your projection table, you may need a wide angle projector.

***Note:**: Take care that your projector is not too bright, otherwise it will be blending you.

There are two ways of mounting your projector inside the case: with a mirror or vertically so it points directly at the glass slab. If you use a mirror or not depends on your projector and on how your housing is constructed.

#### 2.3 Housing with glass slab

The housing can have any shape you like. Here, once again there are a few things to consider.

First, you should get a clear idea, how you want to use it. Should people stand while presenting or sit? Will kids be using it too? Might wheelchair users want to use it? All of these questions influence the design and the measurements of your projection table.

![](img/hardware/1000000000000907000001A51151CD7EF8492335.jpg)

![](img/hardware/100000000000076B000003C01C58B5ECA28A3AE0.jpg)

![](img/hardware/1000000000000907000001A51151CD7EF8492335.jpg)

![](img/hardware/1000000000000907000001A51151CD7EF8492335.jpg)

![](img/hardware/10000000000001C2000001C2C0A4650359B2273A.jpg)

![](img/hardware/10000000000001C2000001C2069B622584CE137B.jpg)

Furthermore, you should think about the size of the projects you want to present. Bigger does not always mean better. If your table is too big, it might not be usable any more. Remember, you should be able to reach everything on the table without changing your position.

![](img/hardware/1000000000000800000006008BF37431F96E90EF.jpg)

####  2.4 Standing position

The perfect height is between 90 and 110 cm / 35 and 43 in, the maximum size should be no bigger than 60×100 cm / 23.5×40 in.

It is also a good idea to have the housing inclined on at least one side, so you can stand closer to it. Otherwise you can also let the work surface overlap for around 10 cm / 4 in or construct arecessed base, similar to what is used for kitchen counters.

![](img/hardware/10000000000003E8000002EE1D9074871E142908.jpg)

####  2.5 Sitting position

To use your projection table in a sitting position, it should not be higher than 75 cm / 29.5 in. This time it is not enough to incline one side, because your legs need more space in a sitting position than while standing. You should consider constructing a real table, or build your projection table into an existing worktable.

Usually, this height is also suited for children. Just emember that kids cannot reach as far with their arms, so* *60×100* *cm /* *23.5×40* *in is already way too big.**

![](img/hardware/1000000000000F9000000A61DEADAE85E7D4F3F0.jpg)


### 3. Glass slab

The glass slab will be used as the navigation area. Later on, you will place your tangible objects on the glass slab to navigate through the project. Therefore, the glass slab should not be too thick; between 4 and 6 mm / 0.16 and 0.24 inch is ideal.

Use transparent glass; to avoid reflections on the glass surface you could use reflection free glass.

For safety reasons, use toughened safety glass to avoid splintering in the case of accident.

***Note:** Plexiglas is not suitable because it scratches easily.

![](img/hardware/1000000000000A3800000621C820F8BDB4237B7F.jpg)

#### 3.1 Size of the glass slab

It is a good idea to have some kind of frame around your projection area. This way, you have some space to put your hands or objects that you don' t need at the moment. If you don' t want to build a frame around the glass slab, you can also just set the projector so the picture is a bit smaller than the glass slab. If you do this, turn off the outside recognition in the edddison editor.

 ![](img/hardware/1000000000000C9000000858D8C374825652625F.jpg)
 
### 4. Rear projection film

The rear projection film enables the projection to be seen on the glass slab. However, it must be see through enough that the camera can still recognize your markers.

***Note:** The rear projection film is put on the side that is facing the projector.*

![](img/hardware/100000000000133000000AC888DCD4E45487BBBE.jpg)

#### 4.1 Apply the rear projection film

To apply the rear projection film correctly, you need the following tools: a spray bottle, spray liquid (mixture of water and a few drops of dish-washing detergent) and a felt-tip squeegee.

First, make sure the glass is free from dust, dirt or oil, then moisten the cleaned glass plate with the spray liquid.

![](img/hardware/10000000000011C8000009D80A9CBE248BA9F123.jpg)

Now remove the film from the release liner (place the film face down on the table and carefully pull away the release liner). Next, spray the
exposed adhesive surface of the film.

***Note:** In order to get the best result, find someone to help you with this.*


Finally, lay the film on the plate (the adhesive surface on the glass) and position it correctly.

Make sure that there is enough liquid between the film and the plate so the film can be moved back and forth.

![](img/hardware/1000000000000CB00000075847313BED3A61651F.jpg)

Use the squeegee to press the film onto the plate. Always start from the middle and push toward the edges. This will squeeze any air and liquid from under the film out to the sides.

Sometimes, the adhesive can leave milky streaks. These will go away on their own once the liquid under the film has evaporated. The same is true for small air bubbles. Larger bubbles have to be popped with a needle.

Now let the glass slab dry for at least 12 hours.Afterwards, you can trim the film to fit the shape of the plate.

![](img/hardware/100000000000133000000AC88F3277682455C0D0.jpg)

If you want to remove the film again, heat it briefly with a hairdryer and then start to peel the film away from one of the corners.

![](img/hardware/1000000000000CCB0000133008EB302D88358EF7.jpg)

### 5. LED IR stripes

The LED infrared stripes are used to light your table from the inside. The camera included in the edddison projection kit has a daylight filter, so the projection is invisible to the camera. The markers on the glass plate are illuminated with the included LED IR stripes. Mount the LED stripes ca. 30cm / 12 in under the glass slab and direct them to the glass, so the whole surface is lighted.

![](img/hardware/100000000000022F0000019504895E9E63DC8AE3.jpg)

### 6. Illumination

You have to ensure that there is homogeneous illumination over a relatively large area. The image recognition system calculates the average of bright and dark spots in the preview and adjusts the brightness on the basis of this value. If a bright spot is found, the surrounding area will be made darker and this can lead to recognition errors.

![](img/hardware/100000000000078000000438C9B3E79E1CDC1B8D.jpg)

### 7. Check illumination

* Place a sheet of white paper on the navigation surface so you can see the distribution of the infrared light. Take your mobile phone and use its camera to check whether the LED is working.

* The camera image should not be too bright. The ideal brightness can be seen on the picture above, the picture on the right is far too bright.

***Note:** Some mobile phone cameras have an IR filter and thus do not see the infrared light.*

![](img/hardware/100000000000054800000491130FE458C5FD0F6A.jpg)

### 8. Mount camera

* Mount the camera on the bottom of your projection table. Make sure there is nothing between the camera and the glass slab; otherwise markers might be invisible for the camera. Also, make sure the camera does not cast a shadow on the projection surface.

![](img/hardware/1000000000000640000006765B06F8A68FBA4FD6.jpg)

### 9. Finish the housing



Before putting the glass slab on the housing, mount the chassis connectors from the edddison projection kit. You will need them later on to plug the camera and the projector to your PC. Also, make sure to leave ventilation openings, so your projector *doesn' t* overheat. Furthermore, make sure to put handles on your housing, so you can move it around later on. If you need to move your projection table a lot, wheels might be a good idea, too.

![](img/hardware/100000000000133000000AC8AE1732A8A433913C.jpg)

### 10. How to go on


Now that your projection table is ready, read the tutorial How to use the edddison projection table to learn how to use it. There you will also find a detailed explanation about the brightness settings in the edddison editor.

It can be found in the help section in edddison or on our homepage: [**edddison.com**](https://www.edddison.com/)