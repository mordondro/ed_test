#  Photobook + Tabletop

To control your 3D software with tangible objects, you can use a photobook in combination with edddison.

 ![](img/hardware/10000000000008D6000004A0BC76EB2E524E0077.jpg)
 
![](img/hardware/1000000000000B32000007ECDBAD361A858D73D6.jpg)


#### 3. Floor plans

Floor plans help you to navigate in the 3D scene.

You can export them together with the calibration markers directly from edddison.

These floorplans will later be printed into your photobook.



#### 6. Prepare your project

To use edddison to present your project, you first have to prepare it in your 3D software and in the edddison editor

You can find instructions on how to do this in the tutorials How to design an interactive SketchUp walk through or How to design an interactive Unity 3D walk through, depending on which software you wish to use.

![](img/hardware/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)


#### 7. Save the plan as image

You can print all the markers used in your project directly via the print dialogue.

In the wizard you can decide if you want to print Floor plans, Object markers or Calibration markers. You will need the floor plans and your generated object markers.

![](img/hardware/100000000000050200000374586981F185CA0B6B.jpg)

First print the floor plans and save them as images in jpg format.

To do that click „ created floor plans" and then open the „print preview".
Here you can „export" your floor plans as images.

![](img/hardware/10000000000005020000037436C8B856393D43BB.jpg)


![](img/hardware/1000000000000487000002BC50FF0843B58688DA.jpg)

#### 10. Look for your preferred Photobook shop

Look for your preferred photobook shop and download their photo book software to order directly online or go to your nearest store and ask for help.

![](img/hardware/100000000000064000000324F09A60D7EF248D30.jpg

#### 11. Design your book

Design the book, import plans, that you exported before as images.
Choose the biggest photobook you can find. (Normally 28×21cm)

![](img/hardware/10000000000003850000028540875F4B76897322.jpg)

#### 12. Lay Flat binding

***Important:** Order "Hardcover with lay flat
binding".*
![](img/hardware/10000000000008D6000004A03D3325D732B41FEE.jpg)

#### 13. Finished!

Now when your book arrives you just lay it underneath the tabletop kit and start your edddison project.

![](img/hardware/10000000000001C2000001C2C0A4650359B2273A.jpg)

As always use the before printed object markers to navigate on your new
photobook!