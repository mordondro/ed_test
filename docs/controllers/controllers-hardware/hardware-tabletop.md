# Controller

## Hardware Controllers

### Tabletop

How to use edddison tracking for a camera kit, a tabletop and a webcam

 ![](img/hardware/10000000000004000000028034AB6BA6C04C4C3F.jpg)
 
#### 1. What you need

1. What you need to control your 3D software with real objects.
1. PC running 3D software and edddison.
2. Presentation screen.
3. Webcam or industrial cam
4. Tangible objects: 3dprinted, laser cutted or use any object that is available, e.g. toys
5. Printed plan with calibration markers.

####  2. Use Cases for edddison Tabletop 

##### 2.1. camera-stand

![](img/hardware/1000000000000400000002806F123181397391E3.jpg)


##### 2.2. ceiling-mounted cam

![](img/hardware/100000000000040000000280DA30EFBB4CBAD050.jpg)



##### 2.3. wall-mounted cam
![](img/hardware/100000000000040000000280393A1D1C49C6BC8C.jpg)

##### 2.4. photobook and wallmounted cam

![](img/hardware/1000000000000400000002809EF05EF3044C4F8E.jpg)



##### 2.5. notebook and webcam
![](img/hardware/1000000000000400000002809963B55AF45B2ADD.jpg)

####  3. Device configuration

![](img/hardware/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)

Plug your camera and in edddison click „**Add Device**".

**Note:** You can only use one device. If you already added another one delete it first.

![](img/hardware/1000000000000500000002D08184463FFCB57AA5.jpg)

##### 3.1 Add a device

A wizard will guide you.

![](img/hardware/1000000000000510000002E409AC171B413D9A2E.jpg)

##### 3.2 Choose a device

Choose „**edddison tabletop** 

![](img/hardware/1000000000000500000002D095442FC0AFD27B2F.jpg)


#####  3.3 Choose your camera

your camera from the hardware list.

#### 4. Prepare floor plans

In order to use your camera you need a floor plan. In edddison click the „Print plans and markers" icon.

![](img/hardware/100000000000047E000003419D11BB31193CC68C.jpg)

When printing use opaque paper or glue the markers to a non-transparent object. If the paper is too thin, the contrast between black and white will be reduced, thus making recognition more difficult. A good choice are (round) self-adhesive stickers.

#### 5. Marker size

Depending on the format of your floor plan, the preset marker size will change. The smaller the format, the smaller the markers will be.

The necessary marker diameter depends on the quality of your camera and the lighting conditions.

![](img/hardware/1000000000000510000002E4BBEEBB1BC14250A8.jpg)

Adjust the marker size for your requirements.

**App benchmarks in mm for**

* Cheap webcams: navigation area up to 500, marker diameter up to 40
good quality webcams: navigation area up to 900, marker diameter up to 45

* Use industrial professional cameras for bigger floor plans or smaller markers

![](img/hardware/1000000000000400000002ABAD2CE65AE04BF460.jpg)

#### 6. Stick markers on tangible objects

The little number on the marker has to look in the same
direction as the object.

![](img/hardware/1000000000000150000002573DD186EA3FD60A23.jpg)

####  7. Properties

* The video image of the camera should not be too dark or bright. Change the „Lightness" value to adjust the image.

* Depending on your camera, you have access to „**Advanced camera settings**" directly within edddison.

* Choose if the webcam should track markers outside the navigation area or not by adjusting the „**Recognition Outside**" value.

* Adjust the "**Jitter compensation**" to tune the sensibility of camera movement or shaking.

* For using high objects, you have to do a height calibration first. The max. height for tangible objects is 7 cm / 2.75 inch max. This should be the same for all used objects.

To calibrate the height of your tangible objects, click the button and follow the **Camera height calibration** wizard.

![](img/hardware/100000000000033E00000202D47F8ACEC034484D.jpg)

#### 8. Camera height calibration

Follow the wizard, the **Mixed Reality Interface** preview window will facilitate the setup.

##### 8.1 Place Calibration Sheet

Place the printed calibration sheet under your camera and make sure it's 
captured entirely.

![](img/hardware/10000000000002EE0000023E136FC15CA689132E.jpg)

Remove all other objects within the camera\'s field of view and click  **Start Calibration**".

![](img/hardware/100000000000033E0000020276520AB82F0DFDCF.jpg)


##### 8.2 Calibration Top Right

Place your control object on the top right corner marker so it\'s covering the marker completely.

When done click **Calibrate Top Right**

![](img/hardware/100000000000033E00000202DB96939AC6C596C7.jpg)


##### 8.3 Calibration Bottom Left

Do the same for the opposite corner.

![](img/hardware/100000000000033E000002024BADB663F4140B8A.jpg)


##### 8.4  Finish Calibration

Click „**Ok**" to finis the calibration.
![](img/hardware/100000000000033E0000020261ED178003CE26F9.jpg)

#### 9. Navigate your scene

![](img/hardware/10000000000002CD000001C2A2EF94EFC396DC40.jpg)

For in-depth documentation on how to navigate your favorite 3D software, read the [corresponding tutorials](https://www.edddison.com/downloads-tutorials/?category=74)[.](https://www.edddison.com/downloads-tutorials/?category=74)

#### 10. Troubleshooting

**Lighting conditions:** If it is too bright, too     dark or the light is not equally distributed on the floor plan, the camera will not recognize all the markers and therefore the tabletop will not work correct.

*Example:* a hot spot on an edddison tabletop caused by the sun. The marker in the bright area will not be recognized.

![](img/hardware/1000000000000535000002C39883A0AF5F9DD275.jpg)


**Camera sharpness:** Check the sharpness of the camera. A sharp focus is important.


![](img/hardware/100000000000018400000122C6D5D4F2CA8772A4.jpg)


#### 11. TIPS

##### 11.1 Printer

 ![](img/hardware/10000000000004300000061859716A37304E58B1.jpg)

** To print plans and markers a laser printer is best. Sometimes the pigments applied by a laser printer have a rich, deeper black than those of an ink jet printer.

* Dirty markers can result in poor tracking. Change the markers from time to time.

* Whenever possible, attach the markers to somewhat larger objects, because a white edge around the marker can improve the consistency of recognition.

* To plug cameras which are mounted too far to use standard USB connections, use an active USB repeater cable.


##### 11.2 When floorplan is on keyboard
![](img/hardware/1000000000000400000002800B6761E4670F7E39.jpg)

* Fix your plans on a stiff board with small bases underneath (to prevent keys to be pushed, see picture below). Place it on top of the keyboard.

![](img/hardware/100000000000133000000AC826A92AF7E957D374.jpg)

##### 11.3 Use a wide angle webcam

* Use a wide angle webcam, e.g. Genius Full HD 1080p.

![](img/hardware/10000000000001F400000112A01A8333C04EFCEC.jpg)