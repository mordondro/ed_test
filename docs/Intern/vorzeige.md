# Design-Fragen

## Frage 1:

Leider ist es nicht moeglich, in Aufzaehlungslisten (mit Nummern) direkt ein Bild nachzustellen, da dadurch die Nummerierung wieder von vorne anfaengt, daher gebe ich euch zwei Optionen, wie wir es machen koennen.

Beispiel:

1. blabla (See Image)

![](img/controller/adddevice.jpg)


3. blabla (See Image)



**Option 1:**

1. blabla (See Image)
2. blabla (See Image)

![](img/controller/adddevice.jpg)

*Step1*

![](img/controller/adddevice.jpg)

*Step2*

**Option 2:**


* blabla 1

![](img/controller/adddevice.jpg)

* blabla 2

![](img/controller/adddevice.jpg)


1. blabla (See Image)

![](img/controller/adddevice.jpg)


3. blabla (See Image)



# Schummelzettel:


# H1
## H2
### H3
#### H4
##### H5
###### H6


Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

1. First ordered list item
2. Another item

* Unordered list.
* Unordered list. 

- Unordered list. 
- Unordered list. 

+ Unordered list. 
+ Unordered list. 

---------------

[I'm an inline-style link](https://www.google.com)

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


<a href="http://www.youtube.com/watch?feature=player_embedded&v=YOUTUBE_VIDEO_ID_HERE
" target="_blank"><img src="http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)
```


```


