[]{#anchor-292}WebGL alpha
--------------------------

### 

### 

### ![](img/webgl/1000000000000191000000F4ED61EBDE8883A627.jpg)[]{#anchor-293}1. Install software

##### 

[]{#anchor-294}WebGL runs without any installation in your browser.
Nevertheless, you still need edddison for WebGL installed on your
computer.

[]{#anchor-295}Start the *edddison-alpha-setup- WebGL.exe* and follow
the wizard throughout the installation.

##### 

![](img/webgl/1000000000000191000000F4E1FFA18E24321AE3.jpg)

2. Start edddison

##### 

[]{#anchor-297}Start the edddison editor using the edddison WebGL demo
icon that appeared on your desktop and open the Demo WebGL Project.

### ![](img/webgl/10000000000004340000025DFE9B8BB294831F53.png)[]{#anchor-298}3. Add your tablet

##### 

[]{#anchor-299}Add and configure your tablet in edddison, using the Add
device button. The wizard will lead you through the configuration of
your tablet.

### ![](img/webgl/1000000000000508000002D755018608C115552E.jpg)[]{#anchor-300}4. Connect your browser

##### 

[]{#anchor-301}Open the web-browser on your tablet.

[]{#anchor-302}Type the browser address, as written in the title bar of
edddison, into the web-browser.

[]{#anchor-303}**Note:** Make sure your tablet is in the same network as
your PC.

### ![](img/webgl/1000000000000780000004B0EC1FF07126E962A3.jpg)[]{#anchor-304}5. Select a floor plan view

##### 

[]{#anchor-305}To start the navigation, first select your preferred
floor plan view. You can do this with the three buttons in the left
bottom corner of your browser.

##### 

### ![](img/webgl/1000000000000191000000F4E16F768CC9A8417A.jpg)[]{#anchor-306}6. Start navigation

##### 

[]{#anchor-307}Now you can start the navigation. Simply move around the
red point of view symbol on your tablet.

[]{#anchor-308}You can choose between three different views and navigate
through your project as with the edddison app.

![](img/webgl/1000000000000191000000F457EFFEEE8BC0B657.jpg)[]{#anchor-309}**Note**

[]{#anchor-310}edddison for WebGL is a demo version. In the editor you
can add floor plans and point of views.

[]{#anchor-311}The functions "add movable object" and "add media" might
cause unexpected incidents and might make the program unstable.

### ![](img/webgl/10000000000002CD000001C2A2EF94EFC396DC40.jpg)[]{#anchor-312}7. Further options

##### 

[]{#anchor-313}edddison also allows you to control your objects via
edddison tabletop or the edddison projection table. To find out more
about the options you have with edddison, check out our homepage:
[*edddison.com*](https://www.edddison.com/)