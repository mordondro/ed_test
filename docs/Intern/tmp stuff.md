![](img/vr/1000000000000281000001AADA8173229CD00380.jpg)

Use your own Unity project or download one of our demos from our
[website](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>).

***Note:** Unity might ask you to upgrade the demo project. Accept and continue.*

* [Demo Project](<https://www.edddison.com/downloads-demo-2/?slug=developer-project-files>)

To find out more about how edddison works (cameras, bounding boxes, objects, etc.) follow this Introduction: LINK

### 4.8 How to present your media?


![](img/editor/1000000000000502000002DCC698C9AE1A9EB6D0.jpg)

Media files are not integrated in the floor plans but are separate objects. That means you can access them at any time, no matter which floor plan you are showing at the moment. Simply click on the eye symbol next to your file name to see your data full screen.

Unral:

**Add** an item to edddison.

Create and link the object in edddison editor.

**Remove** an item from edddison.

Remove the object in edddison

editor too, if you do not need it any more.

----------

10. Add the screenshot to your bounding box in the edddison editor.

![](img/unreal/1000000000000502000002DC18E5FC4AE3804FCE.jpg)

-----------


SKETCHUP

#### 4.1 Download the demo project

To see how edddison works, use one of the demo projects from our
[website](https://www.edddison.com/downloads-demo-2/).

The demo projects include the SketchUp file as well as an edddison file.
![](img/sketchup/1000000000000690000003F202A5CFDA52E50CDB.jpg)

-------------

#### 4.2 Start edddison

Start edddison by clicking the red icon.

***Note:** edddison has to be always started inside
SketchUp, there will not be a shortcut on your desktop.*

 ![](img/sketchup/1000000000000510000002E141C34F1A7FDCDBED.jpg)
 
 
#### 4.3 Import demo configuration

Import the edddison file using the Import project button.
After the project is successfully imported, select it and click open.

### 5. Configure the edddison plug-in

![](img/sketchup/1000000000000690000003F60B836DF051F6736E.jpg)

#### 5.1 Start SketchUp

Start SketchUp and load the project that you wish to work
with.

***Note:** Make sure that the edddison toolbar is
installed.*

 ![](img/sketchup/1000000000000690000003F60B836DF051F6736E.jpg)[]
 
 
CONTROLLER: 
![](img/controller/1000020100000502000002DC76769DA084E74A21.png)

Change height, tilt and rotation of your point of view.
Use the magnifier to zoom in and out of your floor plans (Only available if zoom is turned on).


PLANTSIM

**Supported features**

-  Point of view (POV)
-  Target POV
-  Movable or fixed 3D objects
-  Start / stop simulation
-  Zoom features available on touchscreens
-  add video
-  add images
-  POV control (height, turn)
-  POV (multi-touch, follow my finger)

**Limitations**
-  Start / stop simulation
-  Bounding-boxes (Navigation area) are limited to ONE
-  No tilt or zoom with POV control

[Link: view tutorials for edddison apps, hardware and detailed
configuration online](https://www.edddison.com/downloads-tutorials/)

[Link: watch edddison usage videos on
youtube](https://www.youtube.com/user/edddisontoolkit)

-----------

* Click on **Define Bounding Box** in the *edddison dropdown* to publish it to the edddison editor.

### **3. Configure edddison presentation**

#####**3.1 Create edddison configuration**


![](img/plantsim/1000000000000502000002DC9C5BE0F9DD797B21.jpg)

1) In edddison editor click „Create new project" and enter a name.

![](img/plantsim/1000000000000502000002DC9E64B42D352EE9F9.jpg)

2)The edddison editor launches.

![](img/plantsim/1000020100000502000002DC907EE889F38E4F23.png)

**3.2 Add a floor plan**

Click on the bounding-box icon to create a bounding-box. Follow the instructions in the wizard.

##### **3.3 Add a point-of-view**

Click on the POV icon to create a point of view. Follow the instructions in the wizard. Choose between free and target point of view.

If you want both, click on the POV icon again to create a second one.

##### **3.4 Add a 3D object**

Click on the 3D Object icon to create a movable or fixed object. Follow the instructions in the wizard. Select movable or fixed. This can be changed later in the object's properties.

##### **3.5 Add a simulation control**

Click on the 3D Object icon to create the simulation switch. Follow the instructions in the wizard. Select "\#Simulation" as link.

To start a Simulation make the object visible. To switch the simulation speed rotate the object. To stop the simulation remove, make invisible the object.

##### **3.6 Add media**

Click on one of the Media icons to add an image, an image slideshow or a video.

**3.7 Do presentation**

After configuration the edddison playground may look like this.

![](img/plantsim/10000000000005C0000003444C118C9BE64F43A9.jpg)

Open edddison controller

Add edddison input hardware